package com.moneygram.DCAService.utils;

import java.util.HashMap;

import junit.framework.TestCase;

public class DocPlaceholderSubstUtilTest extends TestCase {

	public void testSimpleSubtitution() {
		HashMap<String, String> dictionaryInHashmap = new HashMap<String, String>();
		dictionaryInHashmap.put("agent-copy-header", "AGENT COPY");
		dictionaryInHashmap.put("receive-receipt-title", "MONEYGRAM(R) RECEIVE");
		dictionaryInHashmap.put("date", "Date");
		dictionaryInHashmap.put("time", "Time");
		
		String text = new String("dlaksjdslakdj {dca-label:agent-copy-header} sfdfsdf fsdf {dca-label:receive-receipt-title} fdslfkj fsdfjkl {dca-label:date}sdf fsdfsdf fdsf {dca-label:time}");
		
		String expected = new String("dlaksjdslakdj AGENT COPY sfdfsdf fsdf MONEYGRAM(R) RECEIVE fdslfkj fsdfjkl Datesdf fsdfsdf fdsf Time");
		
		assertEquals(expected, DocPlaceholderSubstUtil.substitute(text, "label", dictionaryInHashmap));
		
	}
	
	public void testMultipleSubstitution(){
		HashMap<String, String> dictionaryInHashmap1 = new HashMap<String, String>();
		dictionaryInHashmap1.put("agent-copy-header", "AGENT COPY");
		dictionaryInHashmap1.put("receive-receipt-title", "MONEYGRAM(R) RECEIVE");
		
		HashMap<String, String> dictionaryInHashmap2 = new HashMap<String, String>();
		dictionaryInHashmap2.put("date", "Date");
		dictionaryInHashmap2.put("time", "Time");
		
		String text = new String("dlaksjdslakdj {dca-label:agent-copy-header} sfdfsdf fsdf {dca-label:receive-receipt-title} fdslfkj fsdfjkl {dca-token:date}sdf fsdfsdf fdsf {dca-token:time} fdkjfsdlfj {dca-label:notInTheDictionary} rewerwer rfs4rwebsdf");
		
		
		String expected1 = new String("dlaksjdslakdj AGENT COPY sfdfsdf fsdf MONEYGRAM(R) RECEIVE fdslfkj fsdfjkl {dca-token:date}sdf fsdfsdf fdsf {dca-token:time} fdkjfsdlfj  rewerwer rfs4rwebsdf");
		assertEquals(expected1,DocPlaceholderSubstUtil.substitute(text, "label", dictionaryInHashmap1));
		
		String expected2 = new String("dlaksjdslakdj {dca-label:agent-copy-header} sfdfsdf fsdf {dca-label:receive-receipt-title} fdslfkj fsdfjkl Datesdf fsdfsdf fdsf Time fdkjfsdlfj {dca-label:notInTheDictionary} rewerwer rfs4rwebsdf");
		assertEquals(expected2,DocPlaceholderSubstUtil.substitute(text, "token", dictionaryInHashmap2)); 
	}
	
	public void testCombinedSubtitution(){
		HashMap<String, String> dictionaryInHashmap1 = new HashMap<String, String>();
		dictionaryInHashmap1.put("agent-copy-header", "AGENT COPY");
		dictionaryInHashmap1.put("receive-receipt-title", "MONEYGRAM(R) RECEIVE");
		
		HashMap<String, String> dictionaryInHashmap2 = new HashMap<String, String>();
		dictionaryInHashmap2.put("date", "Date");
		dictionaryInHashmap2.put("time", "Time");
		
		String text = new String("dlaksjdslakdj {dca-label:agent-copy-header} sfdfsdf fsdf {dca-label:receive-receipt-title} fdslfkj fsdfjkl {dca-token:date}sdf fsdfsdf fdsf {dca-token:time} fdkjfsdlfj {dca-label:notInTheDictionary} rewerwer rfs4rwebsdf");
		
		String firstSubstitution = new String(DocPlaceholderSubstUtil.substitute(text, "label", dictionaryInHashmap1));
		
		String secondSubstitution = new String(DocPlaceholderSubstUtil.substitute(firstSubstitution, "token", dictionaryInHashmap2));
		
		String expected = new String("dlaksjdslakdj AGENT COPY sfdfsdf fsdf MONEYGRAM(R) RECEIVE fdslfkj fsdfjkl Datesdf fsdfsdf fdsf Time fdkjfsdlfj  rewerwer rfs4rwebsdf");
		
		assertEquals(expected, secondSubstitution);
		
	}

}
