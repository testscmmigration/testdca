package com.moneygram.DCAService.utils;

import java.util.List;

import junit.framework.TestCase;

public class ContentRetrievalUtilTest extends TestCase {
	
	private static String url = "http://devcms.moneygram.com/WCM/idcplg";
	
	public void testGetContent(){
		
		String result = null; 
			
		try {
			result = ContentRetrievalUtil.getContent( "WPOE_WELCOME_US", url, 11000 );
		} catch (DCAException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotNull(result);
	}
	
	public void testGetRevisionInfo(){
		
		List<RevisionInfo> result = null;
		
		try {
			result = ContentRetrievalUtil.getRevisionInfo( "WPOE_WELCOME_US", url, 11000 );
		} catch (DCAException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotNull(result);
	}

}
