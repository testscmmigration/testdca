package com.moneygram.DCAService.utils;

import java.io.IOException;
import java.util.HashMap;

import junit.framework.TestCase;

public class XMLParserUtilTest extends TestCase {

	public void testSingleXmlToHashmap() {
		//xml to parse
		String xmlString = new String("<dictionary language='en_US'> <label id='agent-copy-header'>AGENT COPY</label> <label id='receive-receipt-title'>MONEYGRAM(R) RECEIVE</label> <label id='date'>Date</label> <label id='time'>Time</label></dictionary>");
		//Hashmap to compare into assert
		HashMap<String, String> expected = new HashMap<String, String>();
		expected.put("agent-copy-header", "AGENT COPY");
		expected.put("receive-receipt-title", "MONEYGRAM(R) RECEIVE");
		expected.put("date", "Date");
		expected.put("time", "Time");
		
		//Test
		try{
		assertEquals(expected, XMLParserUtil.SingleXmlToHashmap(xmlString));
		}catch(IOException ie){
			ie.printStackTrace();
		}
		
	}

}
