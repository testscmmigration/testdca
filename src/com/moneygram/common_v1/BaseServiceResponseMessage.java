
package com.moneygram.common_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyResponse;
import com.moneygram.webpoe.service.dca_v1.GetAllStateRegulatorInfoResponse;
import com.moneygram.webpoe.service.dca_v1.GetDisclosureTextResponse;
import com.moneygram.webpoe.service.dca_v1.GetReceiptsFormatResponse;
import com.moneygram.webpoe.service.dca_v1.GetStateRegulatorInfoResponse;
import com.moneygram.webpoe.service.dca_v1.GetTextTranslationsResponse;


/**
 * <p>Java class for BaseServiceResponseMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseServiceResponseMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://moneygram.com/common_v1}Header"/>
 *         &lt;element name="error" type="{http://moneygram.com/common_v1}DCAServiceError" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseServiceResponseMessage", propOrder = {
    "header",
    "error"
})
@XmlSeeAlso({
    DynamicContentAssemblyResponse.class,
    GetReceiptsFormatResponse.class,
    GetAllStateRegulatorInfoResponse.class,
    GetTextTranslationsResponse.class,
    GetStateRegulatorInfoResponse.class,
    GetDisclosureTextResponse.class
})
public class BaseServiceResponseMessage {

    @XmlElement(required = true)
    protected Header header;
    protected Error error;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link Error }
     *     
     */
    public Error getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link Error }
     *     
     */
    public void setError(Error value) {
        this.error = value;
    }

}
