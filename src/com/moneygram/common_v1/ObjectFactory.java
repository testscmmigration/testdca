
package com.moneygram.common_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.moneygram.common_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.moneygram.common_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SecurityHeader }
     * 
     */
    public SecurityHeader createSecurityHeader() {
        return new SecurityHeader();
    }

    /**
     * Create an instance of {@link ProcessingInstruction }
     * 
     */
    public ProcessingInstruction createProcessingInstruction() {
        return new ProcessingInstruction();
    }

    /**
     * Create an instance of {@link RoutingContextHeader }
     * 
     */
    public RoutingContextHeader createRoutingContextHeader() {
        return new RoutingContextHeader();
    }

    /**
     * Create an instance of {@link Errors }
     * 
     */
    public Errors createErrors() {
        return new Errors();
    }

    /**
     * Create an instance of {@link BaseServiceResponseMessage }
     * 
     */
    public BaseServiceResponseMessage createBaseServiceResponseMessage() {
        return new BaseServiceResponseMessage();
    }

    /**
     * Create an instance of {@link ClientHeader }
     * 
     */
    public ClientHeader createClientHeader() {
        return new ClientHeader();
    }

    /**
     * Create an instance of {@link BaseServiceRequestMessage }
     * 
     */
    public BaseServiceRequestMessage createBaseServiceRequestMessage() {
        return new BaseServiceRequestMessage();
    }

    /**
     * Create an instance of {@link RelatedError }
     * 
     */
    public RelatedError createRelatedError() {
        return new RelatedError();
    }

    /**
     * Create an instance of {@link Header }
     * 
     */
    public Header createHeader() {
        return new Header();
    }

    /**
     * Create an instance of {@link RelatedErrors }
     * 
     */
    public RelatedErrors createRelatedErrors() {
        return new RelatedErrors();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

}
