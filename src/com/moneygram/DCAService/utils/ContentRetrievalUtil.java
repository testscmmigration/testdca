package com.moneygram.DCAService.utils;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.jaxen.JDOMXPath;


public class ContentRetrievalUtil {

	
	public static String getContent( String key, String url, int timeOut ) throws DCAException {
		
		String result = null;
		Map<String, String> parameters = new HashMap<String, String>();
        
		if( key != null && !key.equalsIgnoreCase("") ){
			
			parameters.put( "IdcService", "GET_FILE" );
			parameters.put( "RevisionSelectionMethod", "LatestReleased" );
			parameters.put( "Rendition", "Primary" );
			parameters.put( "dDocName", key );
			
			try {
				result = HttpRequestUtility.sendRequest(url, timeOut, parameters);
			} catch (Exception e) {
				DCAException dca = new DCAException("An error ocurring with CMS", e);
				throw dca;
			}
		} else {
			DCAException dca = new DCAException("Invalid key");
			throw dca;
		}
        return result;
	}
	
	public static List<RevisionInfo> getRevisionInfo( String key, String url, int timeOut, String baseUrl ) throws DCAException{
		
		String result = null;
		Map<String, String> parameters = new HashMap<String, String>();
		List<RevisionInfo> revisionsInfo = new ArrayList<RevisionInfo>();
		
		if( key != null && !key.equalsIgnoreCase("") ){
			
			String did = getContentDIDById(key, url, timeOut,baseUrl);
			
			if( did == null ){
				return null;
			}
			
			parameters.put( "IdcService", "REV_HISTORY" );
			parameters.put( "docTemplateName", "WPOE_REV_RESULT" );
			parameters.put( "dID", did );
			
			try {
				result = HttpRequestUtility.sendRequest(url, timeOut, parameters);
				SAXBuilder sb = new SAXBuilder();
				Document doc = sb.build(new StringReader(result));
				List<Element> nodes = (List<Element>) JDOMXPath.selectNodes(doc, "//result/item[@dstatus=\"RELEASED\"]");
				
				for( Element e : nodes ){
					revisionsInfo.add(new RevisionInfo(e));
				}
				
				return revisionsInfo;
				
			} catch (Exception e) {
				DCAException dca = new DCAException("An error ocurring with CMS", e);
				throw dca;
			}
		} else {
			DCAException dca = new DCAException("Invalid key");
			throw dca;
		}
	}
	
	private static String getContentDIDById( String contentId, String url, int timeOut,String baseUrl ) throws DCAException{
		String cmsBaseUrl =baseUrl;
		String result = null;
		Map<String, String> parameters = new HashMap<String, String>();
		
        if( contentId != null && !contentId.equalsIgnoreCase("") ){
        	
        	String params = "?IdcService=GET_SEARCH_RESULTS&docTemplateName=WPOE_SEARCH_RESULT&QueryText=dDocName+%3Cmatches%3E+%60" + contentId + "%60"; 
        	
			try {
				result = HttpRequestUtility.sendRequest(cmsBaseUrl + params, timeOut, parameters, HttpRequestUtility.GET_METHOD);
				SAXBuilder sb = new SAXBuilder();
				Document doc = sb.build(new StringReader(result));
				Element node = (Element)JDOMXPath.selectSingleNode(doc, "//result/item");
				if(node != null){
					return (String)node.getAttributeValue("did");
				}
			} catch (Exception e) {
				DCAException dca = new DCAException("An error ocurring with CMS", e);
				throw dca;
			}
        }
		return null;
    }
	
	public static String getLatestdId(List<RevisionInfo> revInfo){
		String latestrevision=null;
		
		Date latestDate = null;
		
		for(int i=0;i<revInfo.size();i++){
			
			if(latestDate != null){
				Date currentDate = revInfo.get(i).getReleaseDate();
				if(currentDate.compareTo(latestDate) > 0){
					latestDate = currentDate;
					latestrevision = revInfo.get(i).getdRevisionID();
				}
			}else{
				latestDate = revInfo.get(i).getReleaseDate();
				latestrevision = revInfo.get(i).getdRevisionID();
			}
			
		}
		
		return latestrevision;
		
	}

}
