package com.moneygram.DCAService.utils;

public class DCAException extends Exception {

	public DCAException() {
		super();
	}
	
	public DCAException(String message) {
		super(message);
	}

	public DCAException(String message, Throwable cause) {
		super(message, cause);
	}

}
