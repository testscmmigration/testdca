/*
 * Created on Apr 22, 2011
 *
 */
package com.moneygram.DCAService.utils;

import java.util.logging.Logger;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * Spring's Application Context Provider.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>PortalPlatformWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2011/05/24 19:00:19 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ApplicationContextProvider implements ApplicationContextAware, DisposableBean {
	private static final Logger logger = Logger.getLogger(ApplicationContextProvider.class.getName());

	private static ApplicationContext applicationContext = null;

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        applicationContext = ctx;
    }

    public static ApplicationContext getApplicationContext() {
    	return applicationContext;
    }

	public void destroy() throws Exception {
		applicationContext = null;
		logger.info("destroy: removed static reference to the ApplicationContext");
	}

    public static Object getBean(String name) throws RuntimeException {
        if (applicationContext != null) {
            return applicationContext.getBean(name);
        } else {
            throw new IllegalArgumentException("Failed to get a reference to bean name="+ name +": applicationContext is null");
        }
    }

}
