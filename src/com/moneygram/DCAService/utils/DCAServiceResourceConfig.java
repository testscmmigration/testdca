/*
 * Created on Nov 27, 2007
 *
 */
package com.moneygram.DCAService.utils;

/**
 * 
 * Web POE specific Resource Config. <div>
 * <table>
 * <tr>
 * <th>Title:</th>
 * <td>PortalPlatformWeb</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2011</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.19 $ $Date: 2012/02/24 19:18:46 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: uu78 $ </td>
 *         </table>
 *         </div>
 */
public class DCAServiceResourceConfig extends ResourceConfig {
	public static final String MONEY_ORDER_URL = "MONEY_ORDER_URL";
	public static final String CMS_URL = "CMS_URL";
	public static final String CMS_TIMEOUT = "CMS_TIMEOUT";
	public static final String LDAP_URL = "LDAP_URL";
	public static final String LDAP_ADMIN_PASSWORD = "LDAP_ADMIN_PASSWORD";
	public static final String LDAP_ADMIN_BIND_DN = "LDAP_ADMIN_BIND_DN";
	public static final String LDAP_USER_CONTEXT = "LDAP_USER_CONTEXT";
	public static final String URLOPENAM_TOKEN_VALID = "URLOPENAM_TOKEN_VALID";
	public static final String URLOPENAM_AUTHENTICATION = "URLOPENAM_AUTHENTICATION";
	public static final String URLOPENAM_SIGNOUT = "URLOPENAM_SIGNOUT";
	public static final String OPENAM_REALM ="OPENAM_REALM";
	public static final String SENDER_EMAIL_ADDRESS = "SENDER_EMAIL_ADDRESS";
	
	public static final String CORE_APPS_URL="CORE_APPS_URL";
	public static final String AGENT_LOCATOR_URL="AGENT_LOCATOR_URL";
	public static final String CODE_4_FORM_URL ="CODE_4_FORM_URL";

	public static final String KEY_STORE_PATH = "KEY_STORE_PATH"; //path to the keystore 
	public static final String KEY_STORE_PASSWORD = "KEY_STORE_password"; //keystore password (keep _password in lowercase for WAS)
	public static final String KEY_ALIAS = "KEY_ALIAS"; //keystore alias for the key
	public static final String KEY_PASSWORD = "KEY_password"; //key password
	public static final String PASSWORD_EXPIRATION_DAYS = "PASSWORD_EXPIRATION_DAYS";
	
	public static final String 	AGENT_CONNECT_URL="AGENT_CONNECT_URL";
	public static final String 	AGENT_CONNECT_NAMESPACE_URI="AGENT_CONNECT_NAMESPACE_URI";
	public static final String 	AGENT_CONNECT_LOCALPART="AGENT_CONNECT_LOCALPART";
	public static final String 	AGENT_CONNECT_API_VERSION="AGENT_CONNECT_API_VERSION";
	public static final String 	AGENT_CONNECT_SW_VERSION="AGENT_CONNECT_SW_VERSION";
	public static final String 	AGENT_CONNECT_ENABLED="AGENT_CONNECT_ENABLED";
	
	public static final String 	DLS_CONNECT_URL="DLS_CONNECT_URL";
	public static final String 	DLS_CONNECT_NAMESPACE_URI="DLS_CONNECT_NAMESPACE_URI";
	public static final String 	DLS_CONNECT_LOCALPART="DLS_CONNECT_LOCALPART";
	public static final String 	DLS_CONNECT_API_VERSION="DLS_CONNECT_API_VERSION";
	public static final String 	DLS_CONNECT_SW_VERSION="DLS_CONNECT_SW_VERSION";
	public static final String 	DLS_CONNECT_ENABLED="DLS_CONNECT_ENABLED";
	
	public static final String MASS_CSV_TEMPLATE_FILE_PATH = "MASS_CSV_TEMPLATE_FILE_PATH";
	public static final String MASS_XLS_TEMPLATE_FILE_PATH = "MASS_XLS_TEMPLATE_FILE_PATH";
	
	
	public static final String NO_FINGERPRINT = "NO_FINGERPRINT"; 
	

	public static final String GOMEZ_ENABLED = "GOMEZ_ENABLED";
	
	public  String getMoneygramReceiveUrl(){
		return getAttributeValue(CORE_APPS_URL);
	}
	
	public String getAgentLocatorUrl(){
		return getAttributeValue(AGENT_LOCATOR_URL);
	}

	public String getLdapUrl() {
		return getAttributeValue(LDAP_URL);
	}

	public String getCMSUrl() {
		return getAttributeValue(CMS_URL);
	}

	public int getCMSTimeout() {
		return getIntegerAttributeValue(CMS_TIMEOUT, 15000);
	}

	public String getLdapAdminPassword() {
		return getAttributeValue(LDAP_ADMIN_PASSWORD);
	}

	public String getLdapAdminBindDn() {
		return getAttributeValue(LDAP_ADMIN_BIND_DN);
	}

	public String getLdapUserContext() {
		return getAttributeValue(LDAP_USER_CONTEXT);
	}
	
	public String getUrlTokenValid() {
		return getAttributeValue(URLOPENAM_TOKEN_VALID);
	}
	public String getUrlAuthentication() {
		return getAttributeValue(URLOPENAM_AUTHENTICATION);
	}
	public String getUrlSignout() {
		return getAttributeValue(URLOPENAM_SIGNOUT);
	}
	
	public boolean isGomezEnabled() {
		return Boolean.parseBoolean(getAttributeValue(GOMEZ_ENABLED));
	}
	
	public boolean isNoFingerprintEnabled() {
		return Boolean.parseBoolean(getAttributeValue(NO_FINGERPRINT));
	}
	
}
