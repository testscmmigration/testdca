/*
 * Created on Nov 27, 2007
 *
 */
package com.moneygram.DCAService.utils;

import java.util.Properties;
import java.util.logging.Logger;

import com.moneygram.ree.lib.Config;

/**
 * 
 * Resource Config.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>PortalPlatformWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2011/07/22 19:59:08 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ResourceConfig {
	private static final Logger logger = Logger.getLogger(ResourceConfig.class.getName());

	/**
	 * instance of the resource config.
	 */
	private Config config = null;
	
    /**
     * @return Returns the config.
     */
    public Config getConfig() {
        return config;
    }

    /**
     * @param config The config to set.
     */
    public void setConfig(Config config) {
        this.config = config;
    }

	/**
	 * Returns value of the attribute specified.
	 * @param name name
	 * @return value
	 */
    public String getAttributeValue(String name) {
	    return (getConfig() == null ? null : (String)getConfig().getAttribute(name));
	}

	/**
	 * Returns integer value of the attribute specified.
	 * @param attributeName
	 * @param defaultValue
	 * @return
	 */
    public int getIntegerAttributeValue(String attributeName, String defaultValue) {
        // if default value is not passed in, this must be required attribute, so allow the
        //   parseInt to throw an exception out this time
	    int value = getIntegerAttributeValue(attributeName, Integer.parseInt(defaultValue));
		return value;
	}

    public int getIntegerAttributeValue(String attributeName, int defaultValue) {
		String attributeValue = getAttributeValue(attributeName);
		try {
			return Integer.parseInt(attributeValue);
		} catch (Exception e) {
			logger.info("Attribute " + attributeName
					+ " is not set. Using default of " + defaultValue);
		}
		return defaultValue;
	}

   /**
    * Returns all of the Resource Variables as a map of Properties.
    * @return
    */
   public Properties getProperties() {
       return (getConfig() == null ? null : getConfig().getProperties());
   }

   @Override
	public String toString() {
       StringBuffer buffer = new StringBuffer(super.toString());
       buffer.append(" Properties=").append(getProperties());
       return buffer.toString();
	}
}
