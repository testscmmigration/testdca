package com.moneygram.DCAService.utils;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * This class is a helper to manage substitution of placeholder keys of 
 * some text for create a dynamic document. 
 * @author vm59 (Diego Pereyra)
 * 
 */
public class DocPlaceholderSubstUtil {
	
	/**
	 * This method substitute placeholders into some text with values of a HashMap.
	 * @param template Text to be replaced
	 * @param placeholderKey Key added as reference into placeholder. 
	 * @param contentMap Hashmap with the values for replace into document
	 * @return String with the text completed.
	 */
	public static String substitute(String template, String placeholderKey
			,HashMap<String, String> contentMap) {
		
		  Pattern pattern = Pattern.compile("\\{dca-"+placeholderKey+":(.+?)\\}");
	        Matcher matcher = pattern.matcher(template);
	        StringBuffer buffer = new StringBuffer();
	        while (matcher.find()) {
	                String replacement = contentMap.get(matcher.group(1));
	                if (replacement != null) {
	//	                      matcher.appendReplacement(buffer, replacement);
	                        // see comment 
	                        matcher.appendReplacement(buffer, "");
	                        buffer.append(replacement);
	                }else{
	                	 matcher.appendReplacement(buffer, "");
	                     buffer.append("");
	                }
	        }
        matcher.appendTail(buffer);
        return buffer.toString();
	}

	
		
	
	
}
