package com.moneygram.DCAService.utils;

import java.io.Serializable;
import java.util.Date;

import org.jdom2.Element;


public class RevisionInfo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Date releaseDate;
    private String dID;
    private String dRevisionID;
    private String dRevLabel;
    private boolean released;

    public RevisionInfo(Element node)
    {
        if (node != null){
        	dID = node.getAttributeValue("did");
            dRevisionID = node.getAttributeValue("drevisionid");
            dRevLabel = node.getAttributeValue("drevlabel");
            String releasedValue = node.getAttributeValue("dreleasestate");
            if(releasedValue.equalsIgnoreCase("Y")){
            	released = true;
            }else{
            	released = false;
            }
            String releaseDateStr = node.getAttributeValue("dindate");

            //TODO: Test this!
            releaseDate = new Date(releaseDateStr);
        }
    }

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getdID() {
		return dID;
	}

	public void setdID(String dID) {
		this.dID = dID;
	}

	public String getdRevisionID() {
		return dRevisionID;
	}

	public void setdRevisionID(String dRevisionID) {
		this.dRevisionID = dRevisionID;
	}

	public String getdRevLabel() {
		return dRevLabel;
	}

	public void setdRevLabel(String dRevLabel) {
		this.dRevLabel = dRevLabel;
	}

	public boolean isReleased() {
		return released;
	}

	public void setReleased(boolean released) {
		this.released = released;
	}

}
