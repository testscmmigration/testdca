package com.moneygram.DCAService.utils;

import java.io.Serializable;

public class RevisionCacheKey implements Serializable {
	
	String contentkey;
	String ocmUrl;

	public RevisionCacheKey(String contentkey, String ocmUrl) {
		super();
		this.contentkey = contentkey;
		this.ocmUrl = ocmUrl;
	}

	public String getOcmUrl() {
		return ocmUrl;
	}

	public void setOcmUrl(String ocmUrl) {
		this.ocmUrl = ocmUrl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((contentkey == null) ? 0 : contentkey.hashCode());
		result = prime * result + ((ocmUrl == null) ? 0 : ocmUrl.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RevisionCacheKey other = (RevisionCacheKey) obj;
		if (contentkey == null) {
			if (other.contentkey != null)
				return false;
		} else if (!contentkey.equals(other.contentkey))
			return false;
		if (ocmUrl == null) {
			if (other.ocmUrl != null)
				return false;
		} else if (!ocmUrl.equals(other.ocmUrl))
			return false;
		return true;
	}

	public RevisionCacheKey(String contentkey) {
		super();
		this.contentkey = contentkey;
	}

	public String getContentkey() {
		return contentkey;
	}

	public void setContentkey(String contentkey) {
		this.contentkey = contentkey;
	}

}
