package com.moneygram.DCAService.utils;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * Helper to parse XML files to hashmap 
 * @author vm59 (Diego Pereyra)
 *
 */

public class XMLParserUtil {
	
	/**
	 * This method is to convert a simple XML to hashmap.
	 * The content can be only one level xml file and the
	 * and the identify of the tag can be "id" see example 
	 * @param content XML string
	 * @return HashMap with result.
	 * @throws IOException 
	 * @example
	 * XML needed.
	 * <p>
	 *{@code
	 *<dictionary>
	 * 		<label id=name>Diego Pereyra</label>
	 * 		<label id=age>36</label>
	 * </dictionary>
	 *}
	 * <p>Hashmap returned.</p>
	 * <li>"name"->"Diego Pereyra"</li>
	 * <li>"age"->"36"</li>
	 */
	public static HashMap<String, String> SingleXmlToHashmap(String content) throws IOException {
		SAXBuilder builder = new SAXBuilder();
		HashMap<String, String> map = new HashMap<String, String>();
		Document document;
		try {
			document = (Document) builder.build(new StringReader(content));
			Element rootNode = document.getRootElement();			
			List<Element> list = rootNode.getChildren();
			
			
			
			for (Element element : list) {
				map.put(element.getAttributeValue("id"), element.getText());
			}
			
						
			
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return map;
	}
}
