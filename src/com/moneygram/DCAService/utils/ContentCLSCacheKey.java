package com.moneygram.DCAService.utils;

import java.io.Serializable;

public class ContentCLSCacheKey implements Serializable {
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((contentItemName == null) ? 0 : contentItemName.hashCode());
		result = prime
				* result
				+ ((contentServiceLocation == null) ? 0
						: contentServiceLocation.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result
				+ ((disclosureTeg == null) ? 0 : disclosureTeg.hashCode());
		result = prime
				* result
				+ ((longLanguageCode == null) ? 0 : longLanguageCode.hashCode());
		result = prime * result
				+ ((paperFormat == null) ? 0 : paperFormat.hashCode());
		result = prime * result + ((posType == null) ? 0 : posType.hashCode());
		result = prime * result
				+ ((posVersion == null) ? 0 : posVersion.hashCode());
		result = prime * result
				+ ((province == null) ? 0 : province.hashCode());
		result = prime * result
				+ ((receiptType == null) ? 0 : receiptType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContentCLSCacheKey other = (ContentCLSCacheKey) obj;
		if (contentItemName == null) {
			if (other.contentItemName != null)
				return false;
		} else if (!contentItemName.equals(other.contentItemName))
			return false;
		if (contentServiceLocation == null) {
			if (other.contentServiceLocation != null)
				return false;
		} else if (!contentServiceLocation.equals(other.contentServiceLocation))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (disclosureTeg == null) {
			if (other.disclosureTeg != null)
				return false;
		} else if (!disclosureTeg.equals(other.disclosureTeg))
			return false;
		if (longLanguageCode == null) {
			if (other.longLanguageCode != null)
				return false;
		} else if (!longLanguageCode.equals(other.longLanguageCode))
			return false;
		if (paperFormat == null) {
			if (other.paperFormat != null)
				return false;
		} else if (!paperFormat.equals(other.paperFormat))
			return false;
		if (posType == null) {
			if (other.posType != null)
				return false;
		} else if (!posType.equals(other.posType))
			return false;
		if (posVersion == null) {
			if (other.posVersion != null)
				return false;
		} else if (!posVersion.equals(other.posVersion))
			return false;
		if (province == null) {
			if (other.province != null)
				return false;
		} else if (!province.equals(other.province))
			return false;
		if (receiptType == null) {
			if (other.receiptType != null)
				return false;
		} else if (!receiptType.equals(other.receiptType))
			return false;
		return true;
	}
	String contentItemName;
	String contentServiceLocation;
	
	public ContentCLSCacheKey(String contentItemName,
			String contentServiceLocation, String country,
			String longLanguageCode, String province, String posType,
			String receiptType, String posVersion, String disclosureTeg,String paperFormat) {
		super();
		this.contentItemName = contentItemName;
		this.contentServiceLocation = contentServiceLocation;
		this.country = country;
		this.longLanguageCode = longLanguageCode;
		this.province = province;
		this.posType = posType;
		this.receiptType = receiptType;
		this.posVersion = posVersion;
		this.disclosureTeg = disclosureTeg;
		this.paperFormat = paperFormat;
	}

	public String getContentServiceLocation() {
		return contentServiceLocation;
	}

	public void setContentServiceLocation(String contentServiceLocation) {
		this.contentServiceLocation = contentServiceLocation;
	}
	String country;
	String longLanguageCode;
	String province;
	String posType;
	String receiptType;
	String posVersion;
	String disclosureTeg;
	String paperFormat;
	
	
	public String getPaperFormat() {
		return paperFormat;
	}

	public void setPaperFormat(String paperFormat) {
		this.paperFormat = paperFormat;
	}

	/*public ContentCLSCacheKey(String contentItemName, String country,
			String longLanguageCode, String province, String posType,
			String receiptType, String posVersion, String disclosureTeg,String paperFormat) {
		super();
		this.contentItemName = contentItemName;
		this.country = country;
		this.longLanguageCode = longLanguageCode;
		this.province = province;
		this.posType = posType;
		this.receiptType = receiptType;
		this.posVersion = posVersion;
		this.disclosureTeg = disclosureTeg;
	}*/
	
	public String getContentItemName() {
		return contentItemName;
	}
	public void setContentItemName(String contentItemName) {
		this.contentItemName = contentItemName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLongLanguageCode() {
		return longLanguageCode;
	}
	public void setLongLanguageCode(String longLanguageCode) {
		this.longLanguageCode = longLanguageCode;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getPosType() {
		return posType;
	}
	public void setPosType(String posType) {
		this.posType = posType;
	}
	public String getReceiptType() {
		return receiptType;
	}
	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}
	public String getPosVersion() {
		return posVersion;
	}
	public void setPosVersion(String posVersion) {
		this.posVersion = posVersion;
	}
	public String getDisclosureTeg() {
		return disclosureTeg;
	}
	public void setDisclosureTeg(String disclosureTeg) {
		this.disclosureTeg = disclosureTeg;
	}
	

}
