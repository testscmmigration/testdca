/*
 * Created on Nov 11, 2009
 *
 */
package com.moneygram.DCAService.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Http Request Utility.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>GEOIPProxyWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2012/01/13 12:57:17 $ </td><tr><td>
 * @author   </td><td>$Author: vc11 $ </td>
 *</table>
 *</div>
 */
public abstract class HttpRequestUtility {
    private static final Logger logger = LoggerFactory.getLogger(HttpRequestUtility.class);

    public static final String GET_METHOD = "GET";
    public static final String POST_METHOD = "POST";
    public static final String CHARSET = "UTF-8";

    /**
     * Sends http request using apache http client using POST method.
     * @param url
     * @param timeout
     * @param parameters
     * @return http response
     * @throws Exception
     */
    public static String sendRequest(String url, int timeout, Map<String, String> parameters) throws Exception {
    	logger.info("Inside the send Request method");
        return sendRequest(url, timeout, parameters, POST_METHOD);
    }

    /**
     * Sends http request using apache http client.
     * @param url
     * @param timeout
     * @param parameters
     * @param methodCode
     * @return http response
     * @throws Exception
     */
    public static String sendRequest(String url, int timeout, Map<String, String> parameters, String methodCode) throws Exception {
        String response = null;

        if (logger.isDebugEnabled()) {
            logger.debug("sendRequest: sending request with parameters=" + parameters + " using url=" + url +" method="+ methodCode);
        }
        
        long start = System.currentTimeMillis();
        
        HttpClient client = new HttpClient();
        client.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);

        HttpMethodBase method = createMethod(methodCode, url, parameters);
        method.addRequestHeader("Accept-Charset", CHARSET);
        
        try {
            int statusCode = client.executeMethod(method);

            if (logger.isDebugEnabled()) {
                logger.debug("sendRequest: statusCode="+ statusCode);
            }
            
            //Per documentation: It is vital that the response body is always read regardless of the status returned by the server.
            
            BufferedReader input = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream(), CHARSET));
            
            StringBuffer out = new StringBuffer();
            String line;
            
            while ((line = input.readLine()) != null) {
                out.append(line);
                out.append("\r\n");
            }
            input.close();

            response = out.toString();

            if (statusCode != 200 && statusCode != 202) {
                throw new Exception("Http request failed for url=" + url + " responseCode=" + statusCode + " response=" + response);
            }

        } catch (Exception e) {
            throw new Exception("Failed to send/receive the request/response for parameters=" + parameters
                    + " using url=" + url, e);
        } finally {
            method.releaseConnection();
            long elapsed = System.currentTimeMillis() - start;
            if (logger.isDebugEnabled()) {
				logger.debug("sendRequest: sent request with parameters=" + parameters + " using url=" + url +" method="+ methodCode +" elapsed="+ elapsed);
			}
        }
        
        return response;
    }

    private static HttpMethodBase createMethod(String methodCode, String url, Map<String, String> parameters) throws Exception {
        HttpMethodBase method = null;
        if (GET_METHOD.equals(methodCode)) {
            method = new GetMethod(url);
            if (parameters != null && parameters.size() > 0) {
                NameValuePair[] params = new NameValuePair[parameters.size()];
                Iterator iterator = parameters.keySet().iterator();
                for (int i = 0; iterator.hasNext(); i++) {
                    String name = (String)iterator.next();
                    params[i] = new NameValuePair(name, parameters.get(name));
                }
                method.setQueryString(params);
            }
        } else if (POST_METHOD.equals(methodCode)) {
            method = new PostMethod(url);
            if (parameters != null && parameters.size() > 0) {
                for (String name : parameters.keySet()) {
                    ((PostMethod)method).setParameter(name, parameters.get(name));
                }
            }
        } else {
            throw new Exception("Unsupported http method="+ methodCode);
        }
        return method;
    }

    /**
     * Sends xml umf request to EAS using HttpURLConnection.
     * @param umfRequest
     * @return raw umf response
     * @throws Exception 
     */
    public static String sendRequestViaHttpURLConnection(String url, Map<String, String> parameters) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("sendRequest: sending request with parameters=" + parameters + " using url=" + url);
        }
        
        StringBuffer response = new StringBuffer(); 

        try {
            URL servletUrl = new URL(url);
            HttpURLConnection servletConnection = (HttpURLConnection) servletUrl.openConnection();

            servletConnection.setInstanceFollowRedirects(true);

            // inform the connection that we will send output and accept input
            servletConnection.setDoInput(true);
            servletConnection.setDoOutput(parameters != null && parameters.size() > 0);

            // Don't use a cached version of URL connection.
            servletConnection.setUseCaches(false);
            servletConnection.setDefaultUseCaches(false);

            // Specify the content type that we will send binary data
            //servletConnection.setRequestProperty("Content-Type", "text/xml");

            //set request method
            servletConnection.setRequestMethod("POST");

            if (servletConnection.getDoOutput()) {
                //set html parameters
                PrintWriter output = new PrintWriter(servletConnection.getOutputStream());
                if (parameters != null && parameters.size() > 0) {
                    for (String name : parameters.keySet()) {
                        //in servlet spec 1.4 implemented by IBM a single line should be used separated by "&" 
                        output.print(name +"="+ parameters.get(name) +"&");
                    }
                }
                output.flush();
                output.close();
            }

            int responseCode = servletConnection.getResponseCode();

            if (servletConnection.getDoInput()) {
                BufferedReader input = new BufferedReader(new InputStreamReader(servletConnection.getInputStream()));

                String line;
                while ((line = input.readLine()) != null) {
                    response.append(line);
                }
                input.close();
            }
            if (responseCode != 200 && responseCode != 202) {
                throw new Exception("Http request failed for url=" + url + " responseCode=" + responseCode +" response="+ response);
            }


        } catch (Exception e) {
            throw new Exception("Failed to send/receive the request/response request with parameters=" + parameters + " using url=" + url, e);
        } // end of try-catch

        return (response == null ? null : response.toString());
    }
    
    public static boolean isAjaxRequest(HttpServletRequest request) {
    	return request.getHeader("X-Requested-With") != null;
    }

}
