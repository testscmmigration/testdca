/*
 * Created on Nov 2, 2010
 *
 */
package com.moneygram.DCAService.content;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheElementFactory;
import com.moneygram.common.cache.ContextCacheKey;
import com.moneygram.webpoe.dca.service.proxy.content.ContentServiceProxyImpl;
import com.moneygram.DCAService.utils.ApplicationContextProvider;
import com.moneygram.DCAService.utils.ContentCLSCacheKey;
import com.moneygram.DCAService.utils.ContentRetrievalUtil;
import com.moneygram.DCAService.utils.DCAServiceResourceConfig;
import com.moneygram.DCAService.utils.RevisionCacheKey;
import com.moneygram.DCAService.utils.RevisionInfo;

/**
 * 
 * Content Cache Element Factory.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOPortlets</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2011/05/24 19:00:19 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ContentCacheElementFactory implements CacheElementFactory {
    private static final Logger logger = LoggerFactory.getLogger(ContentCacheElementFactory.class);

    /**
     * 
     * @see com.moneygram.common.cache.CacheElementFactory#createElement(java.io.Serializable)
     */
    public CacheElement createElement(Serializable key) throws Exception {
    	
    	
    	RevisionCacheKey revCacheKey = (RevisionCacheKey)key;
    	String ocmUrl = revCacheKey.getOcmUrl() + "?IdcService=GET_FILE&RevisionSelectionMethod=LatestReleased&Rendition=Primary&dDocName=";
    	String result = null;
        try {
            //retrieve content from the CMS
        	result = ContentRetrievalUtil.getContent( revCacheKey.getContentkey(), ocmUrl, 11000 );
        	
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new CacheElement(key, (Serializable) result);
    }

}
