package com.moneygram.DCAService.content;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.DCAService.utils.ApplicationContextProvider;
import com.moneygram.DCAService.utils.ContentRetrievalUtil;
import com.moneygram.DCAService.utils.DCAServiceResourceConfig;
import com.moneygram.DCAService.utils.RevisionCacheKey;
import com.moneygram.DCAService.utils.RevisionInfo;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheElementFactory;
import com.moneygram.common.cache.ContextCacheKey;

public class RevisionHistoryCacheElementFactory implements CacheElementFactory {

	private static final Logger logger = LoggerFactory.getLogger(ContentCacheElementFactory.class);

    /**
     * 
     * @see com.moneygram.common.cache.CacheElementFactory#createElement(java.io.Serializable)
     */
    public CacheElement createElement(Serializable key) throws Exception {
    	
    	RevisionCacheKey revCacheKey = (RevisionCacheKey)key;
    	
		String revUrl = revCacheKey.getOcmUrl() + "?IdcService=REV_HISTORY&docTemplateName=WPOE_REV_RESULT&dID=";
    	List<RevisionInfo> revisionsInfo = null;
        try {
            //retrieve content from the CMS
        	revisionsInfo = ContentRetrievalUtil.getRevisionInfo(revCacheKey.getContentkey(), revUrl, 1100,revCacheKey.getOcmUrl());
        	
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new CacheElement(key, (Serializable) revisionsInfo);
    }
	
}
