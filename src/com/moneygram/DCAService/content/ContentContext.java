/*
 * Created on Jan 21, 2011
 *
 */
package com.moneygram.DCAService.content;

import java.io.Serializable;

/**
 * 
 * Content Context.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOPortlets</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2011/04/29 20:45:21 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ContentContext implements Serializable {
    private static final long serialVersionUID = 1L;

    private boolean failOnError = true;

    /**
     * Creates new instance of ContentContext.
     */
    public ContentContext() {
        super();
    }
    
    /**
     * Creates new instance of ContentContext.
     * @param failOnError
     */
    public ContentContext(boolean failOnError) {
        super();
        this.failOnError = failOnError;
    }

    public boolean isFailOnError() {
        return failOnError;
    }

    public void setFailOnError(boolean failOnError) {
        this.failOnError = failOnError;
    }
}
