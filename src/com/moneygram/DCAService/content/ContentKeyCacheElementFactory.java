/*
 * Created on Nov 2, 2010
 *
 */
package com.moneygram.DCAService.content;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheElementFactory;
import com.moneygram.common.cache.ContextCacheKey;
import com.moneygram.webpoe.dca.service.proxy.content.ContentServiceProxyImpl;
import com.moneygram.DCAService.utils.ApplicationContextProvider;
import com.moneygram.DCAService.utils.ContentCLSCacheKey;
import com.moneygram.DCAService.utils.ContentRetrievalUtil;
import com.moneygram.DCAService.utils.DCAServiceResourceConfig;

/**
 * 
 * Content Cache Element Factory.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOPortlets</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2011/05/24 19:00:19 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ContentKeyCacheElementFactory implements CacheElementFactory {
    private static final Logger logger = LoggerFactory.getLogger(ContentCacheElementFactory.class);

    /**
     * 
     * @see com.moneygram.common.cache.CacheElementFactory#createElement(java.io.Serializable)
     */
    public CacheElement createElement(Serializable key) throws Exception {
    	
        ContentCLSCacheKey contextCacheKey = (ContentCLSCacheKey)key; 
        
        ContentServiceProxyImpl contentSericeProxy = new ContentServiceProxyImpl();
        String contentKey = contentSericeProxy.getContentKey(contextCacheKey.getContentItemName(),contextCacheKey.getContentServiceLocation(),contextCacheKey.getCountry(),contextCacheKey.getLongLanguageCode(),contextCacheKey.getProvince(),contextCacheKey.getPosType(),contextCacheKey.getReceiptType(),contextCacheKey.getPosVersion(),contextCacheKey.getDisclosureTeg(),contextCacheKey.getPaperFormat());
        System.out.println("Content Key got in cache is : " + contentKey);
        

        return new CacheElement(key, (Serializable) contentKey);
    }

}
