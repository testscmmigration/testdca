package com.moneygram.webpoe.service;

import com.moneygram.webpoe.dca.service.proxy.ProxyException;

public class ProxyValidationException extends ProxyException {

	private static final long serialVersionUID = 2509164042709903379L;
	String messageKey;

	public String getMessageKey() {
		return messageKey;
	}

	public void setMessageKey(String messageKey) {
		this.messageKey = messageKey;
	}
	
	public ProxyValidationException(String messageKey, String message){
		super(message);
		setMessageKey(messageKey);
	}
	
    public ProxyValidationException(String messageKey, Throwable cause) {
        super(cause);
        setMessageKey(messageKey);
    }
}
