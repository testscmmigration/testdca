/*
 * Created on Apr 26, 2011
 *
 */
package com.moneygram.webpoe.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.moneygram.webpoe.dca.util.ResourceConfig;

/**
 * 
 * Abstract Base Service.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>PortalPlatformWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2011/05/05 16:28:57 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */

public abstract class BaseService {
	@Autowired
    private ResourceConfig resourceConfig;

	public ResourceConfig getResourceConfig() {
		return resourceConfig;
	}

	public void setResourceConfig(ResourceConfig resourceConfig) {
		this.resourceConfig = resourceConfig;
	}

}
