package com.moneygram.webpoe.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.Source;
import javax.xml.ws.ServiceMode;

import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.moneygram.common.util.ApplicationContextProvider;
import com.moneygram.service.framework.BaseService;
import com.moneygram.service.framework.bo.EJBResponse;
import com.moneygram.service.framework.router.RequestRouter;

@ServiceMode(value = javax.xml.ws.Service.Mode.PAYLOAD)
@javax.xml.ws.WebServiceProvider(targetNamespace = "http://moneygram.com/webpoe/service/dca_v1", serviceName = "dca_v1", portName = "WebPOEDynamicContentAssemblyServicePort", wsdlLocation = "WEB-INF/wsdl/WebPOEContentService_v1.wsdl")
public class DCAService extends BaseService implements javax.xml.ws.Provider<Source> {
	private static final Logger logger = Logger.getLogger(DCAService.class.getName());
	
	/**
	 * Websphere requires the invoke method to be declared in the actual class to avoid the following error:
	 * <code>
     * WSModuleDescr E   WSWS7027E: JAX-WS Service Descriptions could not be correctly built because of the following error: 
     * javax.xml.ws.WebServiceException: Validation error: Provider must have a public invoke method.  
	 * </code>  
	 * @see com.moneygram.service.framework.BaseService#invoke(javax.xml.transform.Source)
	 */
    public javax.xml.transform.Source invoke(Source request) {
        return super.invoke(request);
    }

	/**
	 * 
	 * @see com.moneygram.service.framework.BaseService#processRequest(java.lang.String)
	 */
    public String processRequest(String request) {
    	if (logger.isLoggable(Level.FINEST)) {
			logger.finest("processRequest: request="+ replaceSensitiveData(request));
		}
        RequestRouter router = (RequestRouter)ApplicationContextProvider.getRootBean();
        AbstractPlatformTransactionManager txManager = (AbstractPlatformTransactionManager) ApplicationContextProvider.getBean("txManager");
        
        TransactionDefinition definition = new DefaultTransactionDefinition();
		TransactionStatus status = txManager.getTransaction(definition );
        
        EJBResponse response = router.process(request);
        if(response.isRollback()){
            txManager.rollback(status);
        } else {
        	txManager.commit(status);
        }

    	if (logger.isLoggable(Level.FINEST)) {
			logger.finest("processRequest: response payload="+ replaceSensitiveData(response.getResponsePayload()));
		}
        
        if (response.isReturnResponseAsException()){
            return getSoapFault(response.getResponsePayload());
        }

        return response.getResponsePayload();
    }

    
    private static String replaceSensitiveData(String data) {
    	String safe = null;
    	if (data != null) {
    		safe = removeTagContent("password", data); 
		}

		return safe;
	}

    private static String removeTagContent(String tag, String data) {
		String safe = data.replaceAll("(<.*"+ tag +">).*(</.*"+ tag +">)", "$1***$2");

		return safe;
	}
}
