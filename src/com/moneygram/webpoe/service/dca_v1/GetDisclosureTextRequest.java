
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.BaseServiceRequestMessage;


/**
 * <p>Java class for GetDisclosureTextRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetDisclosureTextRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceRequestMessage">
 *       &lt;sequence>
 *         &lt;element name="disclosureTextRequestList" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyDisclosureRequestList"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDisclosureTextRequest", propOrder = {
    "disclosureTextRequestList"
})
@XmlRootElement(name = "getDisclosureTextRequest")
public class GetDisclosureTextRequest
    extends BaseServiceRequestMessage
{

    @XmlElement(required = true)
    protected DynamicContentAssemblyDisclosureRequestList disclosureTextRequestList;

    /**
     * Gets the value of the disclosureTextRequestList property.
     * 
     * @return
     *     possible object is
     *     {@link DynamicContentAssemblyDisclosureRequestList }
     *     
     */
    public DynamicContentAssemblyDisclosureRequestList getDisclosureTextRequestList() {
        return disclosureTextRequestList;
    }

    /**
     * Sets the value of the disclosureTextRequestList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicContentAssemblyDisclosureRequestList }
     *     
     */
    public void setDisclosureTextRequestList(DynamicContentAssemblyDisclosureRequestList value) {
        this.disclosureTextRequestList = value;
    }

}
