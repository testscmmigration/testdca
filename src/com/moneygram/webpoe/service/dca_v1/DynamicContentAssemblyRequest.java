
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="poeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="posVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paperFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="longLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="province" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tokenList" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyTokenList" minOccurs="0"/>
 *         &lt;element name="CurrentRevisionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyRequest", propOrder = {
    "poeType",
    "posVersion",
    "paperFormat",
    "longLanguageCode",
    "country",
    "province",
    "tokenList",
    "currentRevisionNumber",
    "locale"
})
@XmlSeeAlso({
    DynamicContentAssemblyDisclosureRequest.class,
    DynamicContentAssemblyReceiptRequest.class
})
public class DynamicContentAssemblyRequest {

    @XmlElement(required = true)
    protected String poeType;
    protected String posVersion;
    protected String paperFormat;
    @XmlElement(required = true)
    protected String longLanguageCode;
    @XmlElement(required = true)
    protected String country;
    protected String province;
    protected DynamicContentAssemblyTokenList tokenList;
    @XmlElement(name = "CurrentRevisionNumber")
    protected String currentRevisionNumber;
    protected String locale;

    /**
     * Gets the value of the poeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoeType() {
        return poeType;
    }

    /**
     * Sets the value of the poeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoeType(String value) {
        this.poeType = value;
    }

    /**
     * Gets the value of the posVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosVersion() {
        return posVersion;
    }

    /**
     * Sets the value of the posVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosVersion(String value) {
        this.posVersion = value;
    }

    /**
     * Gets the value of the paperFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaperFormat() {
        return paperFormat;
    }

    /**
     * Sets the value of the paperFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaperFormat(String value) {
        this.paperFormat = value;
    }

    /**
     * Gets the value of the longLanguageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLongLanguageCode() {
        return longLanguageCode;
    }

    /**
     * Sets the value of the longLanguageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongLanguageCode(String value) {
        this.longLanguageCode = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the province property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvince() {
        return province;
    }

    /**
     * Sets the value of the province property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvince(String value) {
        this.province = value;
    }

    /**
     * Gets the value of the tokenList property.
     * 
     * @return
     *     possible object is
     *     {@link DynamicContentAssemblyTokenList }
     *     
     */
    public DynamicContentAssemblyTokenList getTokenList() {
        return tokenList;
    }

    /**
     * Sets the value of the tokenList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicContentAssemblyTokenList }
     *     
     */
    public void setTokenList(DynamicContentAssemblyTokenList value) {
        this.tokenList = value;
    }

    /**
     * Gets the value of the currentRevisionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentRevisionNumber() {
        return currentRevisionNumber;
    }

    /**
     * Sets the value of the currentRevisionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentRevisionNumber(String value) {
        this.currentRevisionNumber = value;
    }

    /**
     * Gets the value of the locale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Sets the value of the locale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocale(String value) {
        this.locale = value;
    }

}
