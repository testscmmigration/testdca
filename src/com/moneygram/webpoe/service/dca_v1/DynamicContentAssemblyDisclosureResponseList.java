
package com.moneygram.webpoe.service.dca_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyDisclosureResponseList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyDisclosureResponseList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DCADisclosureResponse" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyDisclosureResponse" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyDisclosureResponseList", propOrder = {
    "dcaDisclosureResponses"
})
public class DynamicContentAssemblyDisclosureResponseList {

    @XmlElement(name = "DCADisclosureResponse")
    protected List<DynamicContentAssemblyDisclosureResponse> dcaDisclosureResponses;

    /**
     * Gets the value of the dcaDisclosureResponses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dcaDisclosureResponses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDCADisclosureResponses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicContentAssemblyDisclosureResponse }
     * 
     * 
     */
    public List<DynamicContentAssemblyDisclosureResponse> getDCADisclosureResponses() {
        if (dcaDisclosureResponses == null) {
            dcaDisclosureResponses = new ArrayList<DynamicContentAssemblyDisclosureResponse>();
        }
        return this.dcaDisclosureResponses;
    }

}
