
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyReceiptRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyReceiptRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyRequest">
 *       &lt;sequence>
 *         &lt;element name="receiptType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MD5CheckSum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="additionalLanguages" type="{http://moneygram.com/webpoe/service/dca_v1}AdditionalLanguages" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyReceiptRequest", propOrder = {
    "receiptType",
    "md5CheckSum",
    "additionalLanguages"
})
public class DynamicContentAssemblyReceiptRequest
    extends DynamicContentAssemblyRequest
{

    @XmlElement(required = true)
    protected String receiptType;
    @XmlElement(name = "MD5CheckSum")
    protected String md5CheckSum;
    protected AdditionalLanguages additionalLanguages;

    /**
     * Gets the value of the receiptType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptType() {
        return receiptType;
    }

    /**
     * Sets the value of the receiptType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptType(String value) {
        this.receiptType = value;
    }

    /**
     * Gets the value of the md5CheckSum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMD5CheckSum() {
        return md5CheckSum;
    }

    /**
     * Sets the value of the md5CheckSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMD5CheckSum(String value) {
        this.md5CheckSum = value;
    }

    /**
     * Gets the value of the additionalLanguages property.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalLanguages }
     *     
     */
    public AdditionalLanguages getAdditionalLanguages() {
        return additionalLanguages;
    }

    /**
     * Sets the value of the additionalLanguages property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalLanguages }
     *     
     */
    public void setAdditionalLanguages(AdditionalLanguages value) {
        this.additionalLanguages = value;
    }

}
