
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyDisclosureRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyDisclosureRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyRequest">
 *       &lt;sequence>
 *         &lt;element name="disclosuretag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyDisclosureRequest", propOrder = {
    "disclosuretag"
})
public class DynamicContentAssemblyDisclosureRequest
    extends DynamicContentAssemblyRequest
{

    protected String disclosuretag;

    /**
     * Gets the value of the disclosuretag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisclosuretag() {
        return disclosuretag;
    }

    /**
     * Sets the value of the disclosuretag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisclosuretag(String value) {
        this.disclosuretag = value;
    }

}
