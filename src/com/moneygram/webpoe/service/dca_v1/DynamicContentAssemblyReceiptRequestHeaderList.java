
package com.moneygram.webpoe.service.dca_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyReceiptRequestHeaderList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyReceiptRequestHeaderList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="receiptequestHeader" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyReceiptRequestHeader" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyReceiptRequestHeaderList", propOrder = {
    "receiptequestHeaders"
})
public class DynamicContentAssemblyReceiptRequestHeaderList {

    @XmlElement(name = "receiptequestHeader")
    protected List<DynamicContentAssemblyReceiptRequestHeader> receiptequestHeaders;

    /**
     * Gets the value of the receiptequestHeaders property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receiptequestHeaders property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceiptequestHeaders().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicContentAssemblyReceiptRequestHeader }
     * 
     * 
     */
    public List<DynamicContentAssemblyReceiptRequestHeader> getReceiptequestHeaders() {
        if (receiptequestHeaders == null) {
            receiptequestHeaders = new ArrayList<DynamicContentAssemblyReceiptRequestHeader>();
        }
        return this.receiptequestHeaders;
    }

}
