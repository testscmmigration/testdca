
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for POSType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="POSType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AW"/>
 *     &lt;enumeration value="MGO"/>
 *     &lt;enumeration value="PT"/>
 *     &lt;enumeration value="DW"/>
 *     &lt;enumeration value="DT3"/>
 *     &lt;enumeration value="WalmartTouch"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "POSType")
@XmlEnum
public enum POSType {

    AW("AW"),
    MGO("MGO"),
    PT("PT"),
    DW("DW"),
    @XmlEnumValue("DT3")
    DT_3("DT3"),
    @XmlEnumValue("WalmartTouch")
    WALMART_TOUCH("WalmartTouch");
    private final String value;

    POSType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static POSType fromValue(String v) {
        for (POSType c: POSType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
