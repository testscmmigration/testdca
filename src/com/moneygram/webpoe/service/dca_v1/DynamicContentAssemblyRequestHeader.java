
package com.moneygram.webpoe.service.dca_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyRequestHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyRequestHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="posType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="posVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paperFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locale" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="province" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tokenList" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyTokenList" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MD5CheckSum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentRevisionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyRequestHeader", propOrder = {
    "posType",
    "posVersion",
    "paperFormat",
    "locale",
    "country",
    "province",
    "tokenLists",
    "md5CheckSum",
    "currentRevisionNumber"
})
@XmlSeeAlso({
    DynamicContentAssemblyReceiptRequestHeader.class
})
public class DynamicContentAssemblyRequestHeader {

    @XmlElement(required = true)
    protected String posType;
    @XmlElement(required = true)
    protected String posVersion;
    protected String paperFormat;
    @XmlElement(required = true)
    protected String locale;
    @XmlElement(required = true)
    protected String country;
    protected String province;
    @XmlElement(name = "tokenList")
    protected List<DynamicContentAssemblyTokenList> tokenLists;
    @XmlElement(name = "MD5CheckSum")
    protected String md5CheckSum;
    @XmlElement(name = "CurrentRevisionNumber")
    protected String currentRevisionNumber;

    /**
     * Gets the value of the posType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosType() {
        return posType;
    }

    /**
     * Sets the value of the posType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosType(String value) {
        this.posType = value;
    }

    /**
     * Gets the value of the posVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosVersion() {
        return posVersion;
    }

    /**
     * Sets the value of the posVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosVersion(String value) {
        this.posVersion = value;
    }

    /**
     * Gets the value of the paperFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaperFormat() {
        return paperFormat;
    }

    /**
     * Sets the value of the paperFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaperFormat(String value) {
        this.paperFormat = value;
    }

    /**
     * Gets the value of the locale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Sets the value of the locale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocale(String value) {
        this.locale = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the province property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvince() {
        return province;
    }

    /**
     * Sets the value of the province property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvince(String value) {
        this.province = value;
    }

    /**
     * Gets the value of the tokenLists property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tokenLists property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTokenLists().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicContentAssemblyTokenList }
     * 
     * 
     */
    public List<DynamicContentAssemblyTokenList> getTokenLists() {
        if (tokenLists == null) {
            tokenLists = new ArrayList<DynamicContentAssemblyTokenList>();
        }
        return this.tokenLists;
    }

    /**
     * Gets the value of the md5CheckSum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMD5CheckSum() {
        return md5CheckSum;
    }

    /**
     * Sets the value of the md5CheckSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMD5CheckSum(String value) {
        this.md5CheckSum = value;
    }

    /**
     * Gets the value of the currentRevisionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentRevisionNumber() {
        return currentRevisionNumber;
    }

    /**
     * Sets the value of the currentRevisionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentRevisionNumber(String value) {
        this.currentRevisionNumber = value;
    }

}
