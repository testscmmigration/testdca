
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyStateRegulatorInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyStateRegulatorInfoResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyResponse">
 *       &lt;sequence>
 *         &lt;element name="names" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyStateRegulatorNameList"/>
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyStateRegulatorInfoResponse", propOrder = {
    "names",
    "url",
    "phone"
})
public class DynamicContentAssemblyStateRegulatorInfoResponse
    extends DynamicContentAssemblyResponse
{

    @XmlElement(required = true)
    protected DynamicContentAssemblyStateRegulatorNameList names;
    @XmlElement(required = true)
    protected String url;
    @XmlElement(required = true)
    protected String phone;

    /**
     * Gets the value of the names property.
     * 
     * @return
     *     possible object is
     *     {@link DynamicContentAssemblyStateRegulatorNameList }
     *     
     */
    public DynamicContentAssemblyStateRegulatorNameList getNames() {
        return names;
    }

    /**
     * Sets the value of the names property.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicContentAssemblyStateRegulatorNameList }
     *     
     */
    public void setNames(DynamicContentAssemblyStateRegulatorNameList value) {
        this.names = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

}
