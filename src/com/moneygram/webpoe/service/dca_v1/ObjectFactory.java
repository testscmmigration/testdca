
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.moneygram.webpoe.service.dca_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.moneygram.webpoe.service.dca_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyDisclosureRequest }
     * 
     */
    public DynamicContentAssemblyDisclosureRequest createDynamicContentAssemblyDisclosureRequest() {
        return new DynamicContentAssemblyDisclosureRequest();
    }

    /**
     * Create an instance of {@link GetDisclosureTextResponse }
     * 
     */
    public GetDisclosureTextResponse createGetDisclosureTextResponse() {
        return new GetDisclosureTextResponse();
    }

    /**
     * Create an instance of {@link GetTextTranslationsRequest }
     * 
     */
    public GetTextTranslationsRequest createGetTextTranslationsRequest() {
        return new GetTextTranslationsRequest();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyTranslation }
     * 
     */
    public DynamicContentAssemblyTranslation createDynamicContentAssemblyTranslation() {
        return new DynamicContentAssemblyTranslation();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyStateRegulatorInfoResponseList }
     * 
     */
    public DynamicContentAssemblyStateRegulatorInfoResponseList createDynamicContentAssemblyStateRegulatorInfoResponseList() {
        return new DynamicContentAssemblyStateRegulatorInfoResponseList();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyStateRegulatorInfoResponse }
     * 
     */
    public DynamicContentAssemblyStateRegulatorInfoResponse createDynamicContentAssemblyStateRegulatorInfoResponse() {
        return new DynamicContentAssemblyStateRegulatorInfoResponse();
    }

    /**
     * Create an instance of {@link GetStateRegulatorInfoResponse }
     * 
     */
    public GetStateRegulatorInfoResponse createGetStateRegulatorInfoResponse() {
        return new GetStateRegulatorInfoResponse();
    }

    /**
     * Create an instance of {@link GetAllStateRegulatorInfoResponse }
     * 
     */
    public GetAllStateRegulatorInfoResponse createGetAllStateRegulatorInfoResponse() {
        return new GetAllStateRegulatorInfoResponse();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyTokenList }
     * 
     */
    public DynamicContentAssemblyTokenList createDynamicContentAssemblyTokenList() {
        return new DynamicContentAssemblyTokenList();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyRequest }
     * 
     */
    public DynamicContentAssemblyRequest createDynamicContentAssemblyRequest() {
        return new DynamicContentAssemblyRequest();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyStateRegulatorNameList }
     * 
     */
    public DynamicContentAssemblyStateRegulatorNameList createDynamicContentAssemblyStateRegulatorNameList() {
        return new DynamicContentAssemblyStateRegulatorNameList();
    }

    /**
     * Create an instance of {@link GetStateRegulatorInfoRequest }
     * 
     */
    public GetStateRegulatorInfoRequest createGetStateRegulatorInfoRequest() {
        return new GetStateRegulatorInfoRequest();
    }

    /**
     * Create an instance of {@link LongLanguageCode }
     * 
     */
    public LongLanguageCode createLongLanguageCode() {
        return new LongLanguageCode();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyReceiptRequest }
     * 
     */
    public DynamicContentAssemblyReceiptRequest createDynamicContentAssemblyReceiptRequest() {
        return new DynamicContentAssemblyReceiptRequest();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyDisclosureResponse }
     * 
     */
    public DynamicContentAssemblyDisclosureResponse createDynamicContentAssemblyDisclosureResponse() {
        return new DynamicContentAssemblyDisclosureResponse();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyReceiptResponseList }
     * 
     */
    public DynamicContentAssemblyReceiptResponseList createDynamicContentAssemblyReceiptResponseList() {
        return new DynamicContentAssemblyReceiptResponseList();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyDisclosureRequestList }
     * 
     */
    public DynamicContentAssemblyDisclosureRequestList createDynamicContentAssemblyDisclosureRequestList() {
        return new DynamicContentAssemblyDisclosureRequestList();
    }

    /**
     * Create an instance of {@link GetDisclosureTextRequest }
     * 
     */
    public GetDisclosureTextRequest createGetDisclosureTextRequest() {
        return new GetDisclosureTextRequest();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyStateRegulatorInfoRequest }
     * 
     */
    public DynamicContentAssemblyStateRegulatorInfoRequest createDynamicContentAssemblyStateRegulatorInfoRequest() {
        return new DynamicContentAssemblyStateRegulatorInfoRequest();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyTextTranslation }
     * 
     */
    public DynamicContentAssemblyTextTranslation createDynamicContentAssemblyTextTranslation() {
        return new DynamicContentAssemblyTextTranslation();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyReceiptResponse }
     * 
     */
    public DynamicContentAssemblyReceiptResponse createDynamicContentAssemblyReceiptResponse() {
        return new DynamicContentAssemblyReceiptResponse();
    }

    /**
     * Create an instance of {@link GetReceiptsFormatResponse }
     * 
     */
    public GetReceiptsFormatResponse createGetReceiptsFormatResponse() {
        return new GetReceiptsFormatResponse();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyResponse }
     * 
     */
    public DynamicContentAssemblyResponse createDynamicContentAssemblyResponse() {
        return new DynamicContentAssemblyResponse();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyAllStateRegulatorInfoRequest }
     * 
     */
    public DynamicContentAssemblyAllStateRegulatorInfoRequest createDynamicContentAssemblyAllStateRegulatorInfoRequest() {
        return new DynamicContentAssemblyAllStateRegulatorInfoRequest();
    }

    /**
     * Create an instance of {@link GetAllStateRegulatorInfoRequest }
     * 
     */
    public GetAllStateRegulatorInfoRequest createGetAllStateRegulatorInfoRequest() {
        return new GetAllStateRegulatorInfoRequest();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyStateRegulatorInfoRequestList }
     * 
     */
    public DynamicContentAssemblyStateRegulatorInfoRequestList createDynamicContentAssemblyStateRegulatorInfoRequestList() {
        return new DynamicContentAssemblyStateRegulatorInfoRequestList();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyToken }
     * 
     */
    public DynamicContentAssemblyToken createDynamicContentAssemblyToken() {
        return new DynamicContentAssemblyToken();
    }

    /**
     * Create an instance of {@link GetTextTranslationsResponse }
     * 
     */
    public GetTextTranslationsResponse createGetTextTranslationsResponse() {
        return new GetTextTranslationsResponse();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyReceiptRequestList }
     * 
     */
    public DynamicContentAssemblyReceiptRequestList createDynamicContentAssemblyReceiptRequestList() {
        return new DynamicContentAssemblyReceiptRequestList();
    }

    /**
     * Create an instance of {@link GetReceiptsFormatRequest }
     * 
     */
    public GetReceiptsFormatRequest createGetReceiptsFormatRequest() {
        return new GetReceiptsFormatRequest();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyDisclosureResponseList }
     * 
     */
    public DynamicContentAssemblyDisclosureResponseList createDynamicContentAssemblyDisclosureResponseList() {
        return new DynamicContentAssemblyDisclosureResponseList();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyStateRegulatorName }
     * 
     */
    public DynamicContentAssemblyStateRegulatorName createDynamicContentAssemblyStateRegulatorName() {
        return new DynamicContentAssemblyStateRegulatorName();
    }

    /**
     * Create an instance of {@link AdditionalLanguages }
     * 
     */
    public AdditionalLanguages createAdditionalLanguages() {
        return new AdditionalLanguages();
    }

}
