
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyDisclosureResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyDisclosureResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyResponse">
 *       &lt;sequence>
 *         &lt;element name="longLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="disclosureTextTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="disclosureText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyDisclosureResponse", propOrder = {
    "longLanguageCode",
    "disclosureTextTag",
    "disclosureText"
})
public class DynamicContentAssemblyDisclosureResponse
    extends DynamicContentAssemblyResponse
{

    @XmlElement(required = true)
    protected String longLanguageCode;
    @XmlElement(required = true)
    protected String disclosureTextTag;
    @XmlElement(required = true)
    protected String disclosureText;

    /**
     * Gets the value of the longLanguageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLongLanguageCode() {
        return longLanguageCode;
    }

    /**
     * Sets the value of the longLanguageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongLanguageCode(String value) {
        this.longLanguageCode = value;
    }

    /**
     * Gets the value of the disclosureTextTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisclosureTextTag() {
        return disclosureTextTag;
    }

    /**
     * Sets the value of the disclosureTextTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisclosureTextTag(String value) {
        this.disclosureTextTag = value;
    }

    /**
     * Gets the value of the disclosureText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisclosureText() {
        return disclosureText;
    }

    /**
     * Sets the value of the disclosureText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisclosureText(String value) {
        this.disclosureText = value;
    }

}
