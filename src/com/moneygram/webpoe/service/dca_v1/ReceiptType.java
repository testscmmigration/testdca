
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReceiptType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReceiptType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Amend"/>
 *     &lt;enumeration value="BillPay"/>
 *     &lt;enumeration value="Cancel"/>
 *     &lt;enumeration value="PrepaidCard"/>
 *     &lt;enumeration value="Receive"/>
 *     &lt;enumeration value="Refund"/>
 *     &lt;enumeration value="RewardsEnrollment"/>
 *     &lt;enumeration value="SendAgent"/>
 *     &lt;enumeration value="SendCustomer"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReceiptType")
@XmlEnum
public enum ReceiptType {

    @XmlEnumValue("Amend")
    AMEND("Amend"),
    @XmlEnumValue("BillPay")
    BILL_PAY("BillPay"),
    @XmlEnumValue("Cancel")
    CANCEL("Cancel"),
    @XmlEnumValue("PrepaidCard")
    PREPAID_CARD("PrepaidCard"),
    @XmlEnumValue("Receive")
    RECEIVE("Receive"),
    @XmlEnumValue("Refund")
    REFUND("Refund"),
    @XmlEnumValue("RewardsEnrollment")
    REWARDS_ENROLLMENT("RewardsEnrollment"),
    @XmlEnumValue("SendAgent")
    SEND_AGENT("SendAgent"),
    @XmlEnumValue("SendCustomer")
    SEND_CUSTOMER("SendCustomer");
    private final String value;

    ReceiptType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReceiptType fromValue(String v) {
        for (ReceiptType c: ReceiptType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
