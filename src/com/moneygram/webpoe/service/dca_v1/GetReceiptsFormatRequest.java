
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.BaseServiceRequestMessage;


/**
 * <p>Java class for GetReceiptsFormatRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetReceiptsFormatRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceRequestMessage">
 *       &lt;sequence>
 *         &lt;element name="receiptRequestList" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyReceiptRequestList"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetReceiptsFormatRequest", propOrder = {
    "receiptRequestList"
})
@XmlRootElement(name = "getReceiptsFormatRequest")
public class GetReceiptsFormatRequest
    extends BaseServiceRequestMessage
{

    @XmlElement(required = true)
    protected DynamicContentAssemblyReceiptRequestList receiptRequestList;

    /**
     * Gets the value of the receiptRequestList property.
     * 
     * @return
     *     possible object is
     *     {@link DynamicContentAssemblyReceiptRequestList }
     *     
     */
    public DynamicContentAssemblyReceiptRequestList getReceiptRequestList() {
        return receiptRequestList;
    }

    /**
     * Sets the value of the receiptRequestList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicContentAssemblyReceiptRequestList }
     *     
     */
    public void setReceiptRequestList(DynamicContentAssemblyReceiptRequestList value) {
        this.receiptRequestList = value;
    }

}
