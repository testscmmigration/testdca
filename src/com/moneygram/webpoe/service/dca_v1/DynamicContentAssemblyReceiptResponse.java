
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyReceiptResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyReceiptResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyResponse">
 *       &lt;sequence>
 *         &lt;element name="longLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="receiptText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MD5CheckSum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="receiptType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyReceiptResponse", propOrder = {
    "longLanguageCode",
    "receiptText",
    "md5CheckSum",
    "receiptType"
})
public class DynamicContentAssemblyReceiptResponse
    extends DynamicContentAssemblyResponse
{

    @XmlElement(required = true)
    protected String longLanguageCode;
    @XmlElement(required = true)
    protected String receiptText;
    @XmlElement(name = "MD5CheckSum", required = true)
    protected String md5CheckSum;
    @XmlElement(required = true)
    protected String receiptType;

    /**
     * Gets the value of the longLanguageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLongLanguageCode() {
        return longLanguageCode;
    }

    /**
     * Sets the value of the longLanguageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongLanguageCode(String value) {
        this.longLanguageCode = value;
    }

    /**
     * Gets the value of the receiptText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptText() {
        return receiptText;
    }

    /**
     * Sets the value of the receiptText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptText(String value) {
        this.receiptText = value;
    }

    /**
     * Gets the value of the md5CheckSum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMD5CheckSum() {
        return md5CheckSum;
    }

    /**
     * Sets the value of the md5CheckSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMD5CheckSum(String value) {
        this.md5CheckSum = value;
    }

    /**
     * Gets the value of the receiptType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptType() {
        return receiptType;
    }

    /**
     * Sets the value of the receiptType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptType(String value) {
        this.receiptType = value;
    }

}
