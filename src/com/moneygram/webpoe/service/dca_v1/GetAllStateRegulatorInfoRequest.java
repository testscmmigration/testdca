
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.BaseServiceRequestMessage;


/**
 * <p>Java class for GetAllStateRegulatorInfoRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetAllStateRegulatorInfoRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceRequestMessage">
 *       &lt;sequence>
 *         &lt;element name="stateRegulatorInfoRequest" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyAllStateRegulatorInfoRequest"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetAllStateRegulatorInfoRequest", propOrder = {
    "stateRegulatorInfoRequest"
})
@XmlRootElement(name = "getAllStateRegulatorInfoRequest")
public class GetAllStateRegulatorInfoRequest
    extends BaseServiceRequestMessage
{

    @XmlElement(required = true)
    protected DynamicContentAssemblyAllStateRegulatorInfoRequest stateRegulatorInfoRequest;

    /**
     * Gets the value of the stateRegulatorInfoRequest property.
     * 
     * @return
     *     possible object is
     *     {@link DynamicContentAssemblyAllStateRegulatorInfoRequest }
     *     
     */
    public DynamicContentAssemblyAllStateRegulatorInfoRequest getStateRegulatorInfoRequest() {
        return stateRegulatorInfoRequest;
    }

    /**
     * Sets the value of the stateRegulatorInfoRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicContentAssemblyAllStateRegulatorInfoRequest }
     *     
     */
    public void setStateRegulatorInfoRequest(DynamicContentAssemblyAllStateRegulatorInfoRequest value) {
        this.stateRegulatorInfoRequest = value;
    }

}
