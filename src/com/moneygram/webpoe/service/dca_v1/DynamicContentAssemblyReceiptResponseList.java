
package com.moneygram.webpoe.service.dca_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyReceiptResponseList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyReceiptResponseList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DCAReceiptResponse" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyReceiptResponse" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyReceiptResponseList", propOrder = {
    "dcaReceiptResponses"
})
public class DynamicContentAssemblyReceiptResponseList {

    @XmlElement(name = "DCAReceiptResponse")
    protected List<DynamicContentAssemblyReceiptResponse> dcaReceiptResponses;

    /**
     * Gets the value of the dcaReceiptResponses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dcaReceiptResponses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDCAReceiptResponses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicContentAssemblyReceiptResponse }
     * 
     * 
     */
    public List<DynamicContentAssemblyReceiptResponse> getDCAReceiptResponses() {
        if (dcaReceiptResponses == null) {
            dcaReceiptResponses = new ArrayList<DynamicContentAssemblyReceiptResponse>();
        }
        return this.dcaReceiptResponses;
    }

}
