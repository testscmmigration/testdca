
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.BaseServiceResponseMessage;


/**
 * <p>Java class for GetReceiptsFormatResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetReceiptsFormatResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceResponseMessage">
 *       &lt;sequence>
 *         &lt;element name="responseList" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyReceiptResponseList"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetReceiptsFormatResponse", propOrder = {
    "responseList"
})
@XmlRootElement(name = "getReceiptsFormatResponse")
public class GetReceiptsFormatResponse
    extends BaseServiceResponseMessage
{

    @XmlElement(required = true)
    protected DynamicContentAssemblyReceiptResponseList responseList;

    /**
     * Gets the value of the responseList property.
     * 
     * @return
     *     possible object is
     *     {@link DynamicContentAssemblyReceiptResponseList }
     *     
     */
    public DynamicContentAssemblyReceiptResponseList getResponseList() {
        return responseList;
    }

    /**
     * Sets the value of the responseList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicContentAssemblyReceiptResponseList }
     *     
     */
    public void setResponseList(DynamicContentAssemblyReceiptResponseList value) {
        this.responseList = value;
    }

}
