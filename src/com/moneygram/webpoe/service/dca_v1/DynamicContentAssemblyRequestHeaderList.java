
package com.moneygram.webpoe.service.dca_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyRequestHeaderList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyRequestHeaderList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestHeader" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyRequestHeader" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyRequestHeaderList", propOrder = {
    "requestHeaders"
})
public class DynamicContentAssemblyRequestHeaderList {

    @XmlElement(name = "requestHeader")
    protected List<DynamicContentAssemblyRequestHeader> requestHeaders;

    /**
     * Gets the value of the requestHeaders property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestHeaders property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestHeaders().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicContentAssemblyRequestHeader }
     * 
     * 
     */
    public List<DynamicContentAssemblyRequestHeader> getRequestHeaders() {
        if (requestHeaders == null) {
            requestHeaders = new ArrayList<DynamicContentAssemblyRequestHeader>();
        }
        return this.requestHeaders;
    }

}
