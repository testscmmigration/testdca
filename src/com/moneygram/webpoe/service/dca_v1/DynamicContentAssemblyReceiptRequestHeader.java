
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyReceiptRequestHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyReceiptRequestHeader">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyRequestHeader">
 *       &lt;sequence>
 *         &lt;element name="receiptType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyReceiptRequestHeader", propOrder = {
    "receiptType"
})
public class DynamicContentAssemblyReceiptRequestHeader
    extends DynamicContentAssemblyRequestHeader
{

    @XmlElement(required = true)
    protected String receiptType;

    /**
     * Gets the value of the receiptType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptType() {
        return receiptType;
    }

    /**
     * Sets the value of the receiptType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptType(String value) {
        this.receiptType = value;
    }

}
