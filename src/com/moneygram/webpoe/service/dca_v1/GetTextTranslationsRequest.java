
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.BaseServiceRequestMessage;


/**
 * <p>Java class for GetTextTranslationsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetTextTranslationsRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceRequestMessage">
 *       &lt;sequence>
 *         &lt;element name="longLanguageCodes" type="{http://moneygram.com/webpoe/service/dca_v1}LongLanguageCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetTextTranslationsRequest", propOrder = {
    "longLanguageCodes"
})
@XmlRootElement(name = "getTextTranslationsRequest")
public class GetTextTranslationsRequest
    extends BaseServiceRequestMessage
{

    @XmlElement(required = true)
    protected LongLanguageCode longLanguageCodes;

    /**
     * Gets the value of the longLanguageCodes property.
     * 
     * @return
     *     possible object is
     *     {@link LongLanguageCode }
     *     
     */
    public LongLanguageCode getLongLanguageCodes() {
        return longLanguageCodes;
    }

    /**
     * Sets the value of the longLanguageCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link LongLanguageCode }
     *     
     */
    public void setLongLanguageCodes(LongLanguageCode value) {
        this.longLanguageCodes = value;
    }

}
