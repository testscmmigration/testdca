
package com.moneygram.webpoe.service.dca_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.BaseServiceResponseMessage;


/**
 * <p>Java class for DynamicContentAssemblyTextTranslationsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyTextTranslationsResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceResponseMessage">
 *       &lt;sequence>
 *         &lt;element name="textTranslations" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyTextTranslation" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyTextTranslationsResponse", propOrder = {
    "textTranslations"
})
public class DynamicContentAssemblyTextTranslationsResponse
    extends BaseServiceResponseMessage
{

    @XmlElement(required = true)
    protected List<DynamicContentAssemblyTextTranslation> textTranslations;

    /**
     * Gets the value of the textTranslations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the textTranslations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTextTranslations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicContentAssemblyTextTranslation }
     * 
     * 
     */
    public List<DynamicContentAssemblyTextTranslation> getTextTranslations() {
        if (textTranslations == null) {
            textTranslations = new ArrayList<DynamicContentAssemblyTextTranslation>();
        }
        return this.textTranslations;
    }

}
