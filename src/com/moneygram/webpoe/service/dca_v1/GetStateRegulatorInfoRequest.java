
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.BaseServiceRequestMessage;


/**
 * <p>Java class for GetStateRegulatorInfoRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetStateRegulatorInfoRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceRequestMessage">
 *       &lt;sequence>
 *         &lt;element name="stateRegulatorInfoRequestList" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyStateRegulatorInfoRequestList"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetStateRegulatorInfoRequest", propOrder = {
    "stateRegulatorInfoRequestList"
})
@XmlRootElement(name = "getStateRegulatorInfoRequest")
public class GetStateRegulatorInfoRequest
    extends BaseServiceRequestMessage
{

    @XmlElement(required = true)
    protected DynamicContentAssemblyStateRegulatorInfoRequestList stateRegulatorInfoRequestList;

    /**
     * Gets the value of the stateRegulatorInfoRequestList property.
     * 
     * @return
     *     possible object is
     *     {@link DynamicContentAssemblyStateRegulatorInfoRequestList }
     *     
     */
    public DynamicContentAssemblyStateRegulatorInfoRequestList getStateRegulatorInfoRequestList() {
        return stateRegulatorInfoRequestList;
    }

    /**
     * Sets the value of the stateRegulatorInfoRequestList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicContentAssemblyStateRegulatorInfoRequestList }
     *     
     */
    public void setStateRegulatorInfoRequestList(DynamicContentAssemblyStateRegulatorInfoRequestList value) {
        this.stateRegulatorInfoRequestList = value;
    }

}
