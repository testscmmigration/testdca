
package com.moneygram.webpoe.service.dca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyStateRegulatorInfoRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyStateRegulatorInfoRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="longLanguageCodes" type="{http://moneygram.com/webpoe/service/dca_v1}LongLanguageCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyStateRegulatorInfoRequest", propOrder = {
    "state",
    "longLanguageCodes"
})
public class DynamicContentAssemblyStateRegulatorInfoRequest {

    @XmlElement(required = true)
    protected String state;
    @XmlElement(required = true)
    protected LongLanguageCode longLanguageCodes;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the longLanguageCodes property.
     * 
     * @return
     *     possible object is
     *     {@link LongLanguageCode }
     *     
     */
    public LongLanguageCode getLongLanguageCodes() {
        return longLanguageCodes;
    }

    /**
     * Sets the value of the longLanguageCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link LongLanguageCode }
     *     
     */
    public void setLongLanguageCodes(LongLanguageCode value) {
        this.longLanguageCodes = value;
    }

}
