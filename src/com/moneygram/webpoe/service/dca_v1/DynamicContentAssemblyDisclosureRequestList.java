
package com.moneygram.webpoe.service.dca_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyDisclosureRequestList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyDisclosureRequestList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="disclosureRequest" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyDisclosureRequest" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyDisclosureRequestList", propOrder = {
    "disclosureRequests"
})
public class DynamicContentAssemblyDisclosureRequestList {

    @XmlElement(name = "disclosureRequest")
    protected List<DynamicContentAssemblyDisclosureRequest> disclosureRequests;

    /**
     * Gets the value of the disclosureRequests property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the disclosureRequests property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDisclosureRequests().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicContentAssemblyDisclosureRequest }
     * 
     * 
     */
    public List<DynamicContentAssemblyDisclosureRequest> getDisclosureRequests() {
        if (disclosureRequests == null) {
            disclosureRequests = new ArrayList<DynamicContentAssemblyDisclosureRequest>();
        }
        return this.disclosureRequests;
    }

}
