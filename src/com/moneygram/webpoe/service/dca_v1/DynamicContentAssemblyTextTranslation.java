
package com.moneygram.webpoe.service.dca_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicContentAssemblyTextTranslation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicContentAssemblyTextTranslation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="translationTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="translations" type="{http://moneygram.com/webpoe/service/dca_v1}DynamicContentAssemblyTranslation" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicContentAssemblyTextTranslation", propOrder = {
    "translationTag",
    "translations"
})
public class DynamicContentAssemblyTextTranslation {

    @XmlElement(required = true)
    protected String translationTag;
    protected List<DynamicContentAssemblyTranslation> translations;

    /**
     * Gets the value of the translationTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranslationTag() {
        return translationTag;
    }

    /**
     * Sets the value of the translationTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranslationTag(String value) {
        this.translationTag = value;
    }

    /**
     * Gets the value of the translations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the translations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTranslations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicContentAssemblyTranslation }
     * 
     * 
     */
    public List<DynamicContentAssemblyTranslation> getTranslations() {
        if (translations == null) {
            translations = new ArrayList<DynamicContentAssemblyTranslation>();
        }
        return this.translations;
    }

}
