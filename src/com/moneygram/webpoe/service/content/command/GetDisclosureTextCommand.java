package com.moneygram.webpoe.service.content.command;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;

import com.moneygram.DCAService.utils.ContentCLSCacheKey;
import com.moneygram.DCAService.utils.ContentRetrievalUtil;
import com.moneygram.DCAService.utils.DCAException;
import com.moneygram.DCAService.utils.DocPlaceholderSubstUtil;
import com.moneygram.DCAService.utils.RevisionCacheKey;
import com.moneygram.DCAService.utils.RevisionInfo;
import com.moneygram.DCAService.utils.XMLParserUtil;
import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;
import com.moneygram.common.dao.DAOException;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.ReadCommand;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.webpoe.dca.service.proxy.ProxyException;
import com.moneygram.webpoe.dca.service.proxy.content.ContentServiceProxy;
import com.moneygram.webpoe.dca.service.proxy.content.ContentServiceProxyImpl;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyDisclosureRequest;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyDisclosureRequestList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyDisclosureResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyDisclosureResponseList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyRequestHeader;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyRequestHeaderList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyResponseList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyTextResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyToken;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyTokenList;
import com.moneygram.webpoe.service.dca_v1.GetDisclosureTextRequest;
import com.moneygram.webpoe.service.dca_v1.GetDisclosureTextResponse;



public class GetDisclosureTextCommand  extends ReadCommand{
	
	private String OCM_CONTENT_URL = null;
	
	
	public String getOcmContentUrl(){
		return OCM_CONTENT_URL;
	}
	
	public void setOcmContentUrl(String ocmContentUrl){
		this.OCM_CONTENT_URL = ocmContentUrl;
	}
	
	private String CONTENT_SERVICE_URL=null;
	
	public String getContentServiceLocation(){
		return CONTENT_SERVICE_URL;
	}
	
	public void setContentServiceLocation(String contentServiceLocation){
		this.CONTENT_SERVICE_URL = contentServiceLocation;
	}
	private ContentServiceProxy contentServiceProxy;
	
	public ContentServiceProxy getContentServiceProxy() {
		return contentServiceProxy;
	}

	public void setContentServiceProxy(ContentServiceProxy contentServiceProxy) {
		this.contentServiceProxy = contentServiceProxy;
	}

	@Override
	public BaseServiceResponseMessage getResponseToReturnError() {
		return new GetDisclosureTextResponse();
	}

	@Override
	protected boolean isRequestSupported(BaseServiceRequestMessage request)
			throws CommandException {
		return true;
		//return request instanceof GetDisclosureTextRequest;
	}

	@Override
	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request)
			throws CommandException {
		GetDisclosureTextRequest disclosureTextReq = (GetDisclosureTextRequest) request;
		
		DynamicContentAssemblyDisclosureRequestList reqList = disclosureTextReq.getDisclosureTextRequestList();
		
		List<DynamicContentAssemblyDisclosureRequest>  reqItem = reqList.getDisclosureRequests();
		
		DynamicContentAssemblyDisclosureResponseList respList = new DynamicContentAssemblyDisclosureResponseList();
		GetDisclosureTextResponse response = new GetDisclosureTextResponse();
		
		
		
		for(int i=0;i<reqItem.size();i++)
		{
			//Content Key Service Call
			String contentKey= null;
			String languageKey=null;
			boolean contentExist=true;
			CacheManager cacheManager = null;
			try{
				System.out.println("Inside the command");
				
				cacheManager = CacheManagerFactory.getCacheManagerInstance();
				Cache contentCache = cacheManager.getCache("ContentKeyCache");
				Cache languageCache = cacheManager.getCache("ContentKeyCache");
				
				if (contentCache == null || languageCache == null) {
		            throw new Exception("Failed to access cache ContentCache");
		        }
				ContentCLSCacheKey clsCacheKey = new ContentCLSCacheKey("dca_closure_text",getContentServiceLocation(),reqItem.get(i).getCountry(),reqItem.get(i).getLongLanguageCode(),reqItem.get(i).getProvince(),reqItem.get(i).getPoeType(),null,reqItem.get(i).getPosVersion(),reqItem.get(i).getDisclosuretag(),null);
				
				ContentCLSCacheKey languageCacheKey = new ContentCLSCacheKey("dca_language_dictionary",getContentServiceLocation(),reqItem.get(i).getCountry(),reqItem.get(i).getLongLanguageCode(),reqItem.get(i).getProvince(),reqItem.get(i).getPoeType(),null,reqItem.get(i).getPosVersion(),reqItem.get(i).getDisclosuretag(),reqItem.get(i).getPaperFormat());
				
		        CacheElement contentCacheElement = contentCache.getElement(clsCacheKey);
		        
		        if(contentCacheElement.getValue() != null)
					contentKey = contentCacheElement.getValue().toString();
				else
					contentExist=false;
				
		        CacheElement languageCacheElement = languageCache.getElement(languageCacheKey);
		        
		        if(languageCacheElement.getValue() != null)
		        	languageKey = languageCacheElement.getValue().toString();
		        else
		        	contentExist = false;
		       
				System.out.println("Content Key received in DCA Service : " + contentKey);
				System.out.println("Language Key received in DCA Service : " + languageKey);
				
			}catch(ProxyException pe){
				System.out.println("Proxy Exception in GetDisclosureTextCommand");
				pe.printStackTrace();
			} catch (CacheException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(contentExist){
			
				String result = null;
				String languageSubText=null;
				String substitutedResult = null;
				
				String latestContentRevId = null;
				String latestLanguageRevId = null;
					
				try {
					
					RevisionCacheKey ocmContentKey = new RevisionCacheKey(contentKey,getOcmContentUrl());
					RevisionCacheKey ocmLanguageKey = new RevisionCacheKey(languageKey,getOcmContentUrl());
					
					Cache contentCache = cacheManager.getCache("ContentCache");
					Cache languageCache = cacheManager.getCache("ContentCache");
					
					//Get the content and language substitution from CMS
					result = contentCache.getElement(ocmContentKey).getValue().toString();
					languageSubText = contentCache.getElement(ocmLanguageKey).getValue().toString();
					
					System.out.println("Content got from OCM is : " + result);
					System.out.println("Language got from OCM is : " + languageSubText);
					
					if(languageKey != null){
						HashMap<String,String> languageMap = XMLParserUtil.SingleXmlToHashmap(languageSubText);
						//Language Substitution
						substitutedResult = DocPlaceholderSubstUtil.substitute(result, "label", languageMap);
					}else{
						substitutedResult = result;
					}
				
					//Token Substitution
				    DynamicContentAssemblyTokenList requestTokenList =  reqItem.get(i).getTokenList();
					if(requestTokenList != null){
						DynamicContentAssemblyTokenList requestTokens = (DynamicContentAssemblyTokenList)requestTokenList;
						List tokens = requestTokens.getTokens();
						HashMap<String,String> tokenMap = new HashMap<String,String>();
						
						if(tokens != null){
							for(int j=0;j<tokens.size();j++){
								DynamicContentAssemblyToken requestToken = (DynamicContentAssemblyToken) tokens.get(j);
								tokenMap.put(requestToken.getKey(), requestToken.getValue());
							}
							
							substitutedResult = DocPlaceholderSubstUtil.substitute(substitutedResult, "token", tokenMap);
						}
					}
					
					
					System.out.println("Substituted Text : " + substitutedResult);
					
					int latestContentRevisionId = 0;
					int latestLanguageRevisionId = 0;
					
					//Get the revision information for content and language substitution.
					Cache contentRevCache = cacheManager.getCache("RevisionCache");
					Cache languageRevCache = cacheManager.getCache("RevisionCache");
					
					List<RevisionInfo> contentRevs = (List<RevisionInfo>) contentRevCache.getElement(ocmContentKey).getValue();
					System.out.println("size of content revisions : " + contentRevs.size());
					
					if(languageKey != null){
						List<RevisionInfo> languageRevs = (List<RevisionInfo>) contentRevCache.getElement(ocmLanguageKey).getValue();
						System.out.println("size of content revisions : " + contentRevs.size());
						latestContentRevId = ContentRetrievalUtil.getLatestdId(contentRevs);
						latestLanguageRevId = ContentRetrievalUtil.getLatestdId(languageRevs);
						
						System.out.println("Latest Content Revision Id is : " + latestContentRevId);
						System.out.println("Latest Language Revision Id is : " + latestLanguageRevId);
						latestContentRevisionId = new Integer(latestContentRevId).intValue();
						latestLanguageRevisionId = new Integer(latestLanguageRevId).intValue();
					}
					
					int consumerContentRevisionId =0;
					int consumerLanguageRevisionId=0;
					
					if(reqItem.get(i).getCurrentRevisionNumber() != null){
						StringTokenizer versions = new StringTokenizer(reqItem.get(i).getCurrentRevisionNumber(),".");
						while(versions.hasMoreTokens()){
							consumerContentRevisionId = new Integer(versions.nextToken()).intValue();
							consumerLanguageRevisionId = new Integer(versions.nextToken()).intValue();
							System.out.println("Consumer Content Revision Id is : " + consumerContentRevisionId);
							System.out.println("Consumer Language Revision Id is : " + consumerLanguageRevisionId);
						}
					}else{
						
					}
					
					DynamicContentAssemblyDisclosureResponse resp = new DynamicContentAssemblyDisclosureResponse();
					
					/*//Consumer version is earlier than the latest version - return the full content
					if( latestContentRevisionId > consumerContentRevisionId){
						
						resp.setDisclosureText(substitutedResult);
						resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
					//Consumer version is later than the latest version - return error code.
					} else if(latestContentRevisionId < consumerContentRevisionId){
						if(latestLanguageRevisionId > consumerLanguageRevisionId){
							resp.setDisclosureText(substitutedResult);
							resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
						}else{
							//DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();
							resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
							resp.setErrorCode("DCA_CONSUMER_VERSION");
						}
						//Consumer version is same as the latest version - check the Checksum hash
					} else if(latestContentRevisionId == consumerContentRevisionId){
						if(latestLanguageRevisionId > consumerLanguageRevisionId){
							resp.setDisclosureText(substitutedResult);
							resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
						} else if(latestLanguageRevisionId < consumerLanguageRevisionId){
							resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
							resp.setErrorCode("DCA_CONSUMER_VERSION");
						}else if(latestLanguageRevisionId == consumerLanguageRevisionId){
							resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
							resp.setErrorCode("DCA_NO_CHANGE");
							
						}
					}*/
					
					if(latestContentRevisionId == consumerContentRevisionId && latestLanguageRevisionId == consumerLanguageRevisionId){
						resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
						resp.setErrorCode("DCA_NO_CHANGE");
					}else{
						resp.setDisclosureText(substitutedResult);
						resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
					}
					
					if(reqItem.get(i).getLongLanguageCode() != null)
						resp.setLongLanguageCode(reqItem.get(i).getLongLanguageCode());
					if(reqItem.get(i).getProvince() != null)
						resp.setState(reqItem.get(i).getProvince());
					if(reqItem.get(i).getDisclosuretag() != null)
						resp.setDisclosureTextTag(reqItem.get(i).getDisclosuretag());
					
					
					
					respList.getDCADisclosureResponses().add(resp);
					
				} /*catch (DCAException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
				}*/catch(IOException ie){
					ie.printStackTrace();
				} /*catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/ catch (CacheException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				DynamicContentAssemblyDisclosureResponse resp = new DynamicContentAssemblyDisclosureResponse();
				if(reqItem.get(i).getLongLanguageCode() != null)
					resp.setLongLanguageCode(reqItem.get(i).getLongLanguageCode());
				if(reqItem.get(i).getProvince() != null)
					resp.setState(reqItem.get(i).getProvince());
				if(reqItem.get(i).getDisclosuretag() != null)
					resp.setState(reqItem.get(i).getDisclosuretag());
				if(reqItem.get(i).getCurrentRevisionNumber() != null)
					resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
				resp.setErrorCode("DCA_NOT_FOUND");
				respList.getDCADisclosureResponses().add(resp);
			}
		}
		
		response.setResponseList(respList);
		
		return response;
	}

}
