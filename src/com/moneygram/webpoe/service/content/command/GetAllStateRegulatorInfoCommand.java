package com.moneygram.webpoe.service.content.command;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.jaxen.JDOMXPath;

import com.moneygram.DCAService.utils.ContentCLSCacheKey;
import com.moneygram.DCAService.utils.ContentRetrievalUtil;
import com.moneygram.DCAService.utils.DCAException;
import com.moneygram.DCAService.utils.RevisionCacheKey;
import com.moneygram.DCAService.utils.RevisionInfo;
import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;
import com.moneygram.common.dao.DAOException;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.ReadCommand;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.webpoe.dca.service.proxy.ProxyException;
import com.moneygram.webpoe.dca.service.proxy.content.ContentServiceProxy;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyAllStateRegulatorInfoRequest;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyDisclosureRequest;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyDisclosureRequestList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyResponseList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorInfoRequest;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorInfoRequestList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorInfoResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorInfoResponseList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorName;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorNameList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyTextResponse;
import com.moneygram.webpoe.service.dca_v1.GetAllStateRegulatorInfoRequest;
import com.moneygram.webpoe.service.dca_v1.GetDisclosureTextResponse;
import com.moneygram.webpoe.service.dca_v1.GetStateRegulatorInfoRequest;
import com.moneygram.webpoe.service.dca_v1.GetStateRegulatorInfoResponse;
import com.moneygram.webpoe.service.dca_v1.LongLanguageCode;



public class GetAllStateRegulatorInfoCommand  extends ReadCommand{
	
	private String CONTENT_SERVICE_URL=null;
	
	private String OCM_CONTENT_URL = null;
	
	
	public String getOcmContentUrl(){
		return OCM_CONTENT_URL;
	}
	
	public void setOcmContentUrl(String ocmContentUrl){
		this.OCM_CONTENT_URL = ocmContentUrl;
	}
	
	public String getContentServiceLocation(){
		return CONTENT_SERVICE_URL;
	}
	
	public void setContentServiceLocation(String contentServiceLocation){
		this.CONTENT_SERVICE_URL = contentServiceLocation;
	}
	
	private ContentServiceProxy contentServiceProxy;
	
	public ContentServiceProxy getContentServiceProxy() {
		return contentServiceProxy;
	}

	public void setContentServiceProxy(ContentServiceProxy contentServiceProxy) {
		this.contentServiceProxy = contentServiceProxy;
	}
	
	@Override
	public BaseServiceResponseMessage getResponseToReturnError() {
		return new GetStateRegulatorInfoResponse();
	}

	@Override
	protected boolean isRequestSupported(BaseServiceRequestMessage request)
			throws CommandException {
		return true;
		//return request instanceof GetApplicationsRequest;
	}

	@Override
	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request)
			throws CommandException {
		
		GetAllStateRegulatorInfoRequest stateRegulatorInfoRequest = (GetAllStateRegulatorInfoRequest) request;
		DynamicContentAssemblyAllStateRegulatorInfoRequest reqItem = stateRegulatorInfoRequest.getStateRegulatorInfoRequest();
		
		LongLanguageCode languageCodes = reqItem.getLongLanguageCodes();
		List<String> languages = languageCodes.getLongLanguageCodes();
		
		
		DynamicContentAssemblyStateRegulatorInfoResponseList respList = new DynamicContentAssemblyStateRegulatorInfoResponseList();
		DynamicContentAssemblyStateRegulatorInfoResponse resp = new DynamicContentAssemblyStateRegulatorInfoResponse();
		
		DynamicContentAssemblyStateRegulatorNameList nameList = new DynamicContentAssemblyStateRegulatorNameList();
		DynamicContentAssemblyStateRegulatorName names = new DynamicContentAssemblyStateRegulatorName();
		GetStateRegulatorInfoResponse response = new GetStateRegulatorInfoResponse();
		String result = null;
		CacheManager cacheManager = null;
		try{
			cacheManager = CacheManagerFactory.getCacheManagerInstance();
			Cache contentKeyCache = cacheManager.getCache("ContentKeyCache");
			
			if (contentKeyCache == null) {
	            throw new Exception("Failed to access cache ContentCache");
	        }
			//Content Key Service Call
			String contentKey= null;
			ContentCLSCacheKey clsCacheKey = new ContentCLSCacheKey("dca_state_regulator",getContentServiceLocation(),null,null,"all",null,null,null,null,null);
			CacheElement contentCacheElement = contentKeyCache.getElement(clsCacheKey);
			
			//contentKey = getContentServiceProxy().getContentKey("dca_state_regulator",null,null,"all",null,null,null,null);
			
			if(contentCacheElement.getValue() != null)
				contentKey = contentCacheElement.getValue().toString();
			
			RevisionCacheKey contentRevKey = new RevisionCacheKey(contentKey,getOcmContentUrl());
			
			Cache contentCache = cacheManager.getCache("ContentCache");
			result = contentCache.getElement(contentRevKey).getValue().toString();
			
			//Commenting the call for getting the revision so that we can use it later.
			
			/*Cache contentRevCache = cacheManager.getCache("RevisionCache");
			List<RevisionInfo> contentRevs = (List<RevisionInfo>) contentRevCache.getElement(contentRevKey).getValue();
			System.out.println("size of content revisions : " + contentRevs.size());
			String latestContentRevId = ContentRetrievalUtil.getLatestdId(contentRevs);
			System.out.println("Latest Content Revision Id is : " + latestContentRevId);*/
			
			//result = ContentRetrievalUtil.getContent( contentKey, ocmUrl, 11000 );
			
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(new StringReader(result));
			System.out.println("Got the doc");
			List<Element> stateRegulatorInfoList = (List<Element>) JDOMXPath.selectNodes(doc, "//stateregulatorinfolist/stateregulatorinfo");
			for(int i=0;i<stateRegulatorInfoList.size();i++){
				Element stateregulatorInfoElement = stateRegulatorInfoList.get(i);
				String state = stateregulatorInfoElement.getAttributeValue("state");
				String phone = stateregulatorInfoElement.getChildText("phone");
				String url = stateregulatorInfoElement.getChildText("url");
				System.out.println("State : " + state);
				System.out.println("Phone : " + phone);
				System.out.println("url : " + url);
				nameList = new DynamicContentAssemblyStateRegulatorNameList();
				resp = new DynamicContentAssemblyStateRegulatorInfoResponse();
				for(int j=0;j<languages.size();j++){
					names = new DynamicContentAssemblyStateRegulatorName();
					Element nameListReq = (Element) JDOMXPath.selectSingleNode(doc, "//stateregulatorinfolist/stateregulatorinfo[@state=\"" + state + "\"]/namelist/name[@language=\"" + languages.get(j) + "\"]");
					if(nameListReq != null){
						names.setLongLanguageCode(languages.get(j));
						String name = nameListReq.getText();
						System.out.println("Name got is " + name);
						names.setName(name);
						nameList.getDCARegulatorNames().add(names);
					}
				}
				resp.setNames(nameList);
				resp.setState(state);
				resp.setPhone(phone);
				resp.setUrl(url);
				resp.setRevisionNumber("1.0");
				resp.setErrorCode("None");
					
				respList.getDCAStateRegulatoreInfoResponses().add(resp);
				
			}
			
		}catch(ProxyException pe){
			pe.printStackTrace();
		} catch (DCAException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CacheException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//GetDisclosureTextResponse response = new GetDisclosureTextResponse();
		response.setResponseList(respList);
		
		return response;
	}

}
