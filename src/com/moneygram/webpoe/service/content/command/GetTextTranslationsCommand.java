package com.moneygram.webpoe.service.content.command;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.jaxen.JDOMXPath;

import com.moneygram.DCAService.utils.ContentCLSCacheKey;
import com.moneygram.DCAService.utils.ContentRetrievalUtil;
import com.moneygram.DCAService.utils.DCAException;
import com.moneygram.DCAService.utils.RevisionCacheKey;
import com.moneygram.DCAService.utils.RevisionInfo;
import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;
import com.moneygram.common.dao.DAOException;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.ReadCommand;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.webpoe.dca.service.proxy.ProxyException;
import com.moneygram.webpoe.dca.service.proxy.content.ContentServiceProxy;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyTextTranslation;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyTranslation;
import com.moneygram.webpoe.service.dca_v1.GetTextTranslationsRequest;
import com.moneygram.webpoe.service.dca_v1.GetTextTranslationsResponse;



public class GetTextTranslationsCommand  extends ReadCommand{
	
	private String CONTENT_SERVICE_URL=null;
	
	private String OCM_CONTENT_URL = null;
	
	
	public String getOcmContentUrl(){
		return OCM_CONTENT_URL;
	}
	
	public void setOcmContentUrl(String ocmContentUrl){
		this.OCM_CONTENT_URL = ocmContentUrl;
	}
	
	public String getContentServiceLocation(){
		return CONTENT_SERVICE_URL;
	}
	
	public void setContentServiceLocation(String contentServiceLocation){
		this.CONTENT_SERVICE_URL = contentServiceLocation;
	}
	
	private ContentServiceProxy contentServiceProxy;
	
	public ContentServiceProxy getContentServiceProxy() {
		return contentServiceProxy;
	}

	public void setContentServiceProxy(ContentServiceProxy contentServiceProxy) {
		this.contentServiceProxy = contentServiceProxy;
	}
	
	@Override
	public BaseServiceResponseMessage getResponseToReturnError() {
		return new GetTextTranslationsResponse();
	}

	@Override
	protected boolean isRequestSupported(BaseServiceRequestMessage request)
			throws CommandException {
		return true;
		//return request instanceof GetApplicationsRequest;
	}

	@Override
	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request)
			throws CommandException {
		GetTextTranslationsRequest  textTranslationRequest = (GetTextTranslationsRequest) request;
		GetTextTranslationsResponse response = new GetTextTranslationsResponse();
		System.out.println(" Long Language Codes Size : " + textTranslationRequest.getLongLanguageCodes().getLongLanguageCodes().size());
		String result = null;
		String latestContentRevId = null;
		
		Map<String,String> tags = new HashMap<String,String>();
		for(int i=0;i<textTranslationRequest.getLongLanguageCodes().getLongLanguageCodes().size();i++){
			String contentKey= null;
			System.out.println("Long Language Code : " + textTranslationRequest.getLongLanguageCodes().getLongLanguageCodes().get(i));
			String longLangCode = textTranslationRequest.getLongLanguageCodes().getLongLanguageCodes().get(i).toString();
			CacheManager cacheManager = null;
			try{
				cacheManager = CacheManagerFactory.getCacheManagerInstance();
				Cache contentKeyCache = cacheManager.getCache("ContentKeyCache");
			
				if (contentKeyCache == null) {
		            throw new Exception("Failed to access cache ContentCache");
		        }
				
				
				ContentCLSCacheKey clsCacheKey = new ContentCLSCacheKey("dca_text_translations",getContentServiceLocation(),null,textTranslationRequest.getLongLanguageCodes().getLongLanguageCodes().get(i),null,null,null,null,null,null);
				
				CacheElement contentCacheElement = contentKeyCache.getElement(clsCacheKey);
				
				if(contentCacheElement.getValue() != null)
					contentKey = contentCacheElement.getValue().toString();
				
				if(contentKey != null){ 
					RevisionCacheKey contentRevKey = new RevisionCacheKey(contentKey,getOcmContentUrl());
					
					Cache contentCache = cacheManager.getCache("ContentCache");
					
					//Get the revision information for content.
					Cache contentRevCache = cacheManager.getCache("RevisionCache");
					
					List<RevisionInfo> contentRevs = (List<RevisionInfo>) contentRevCache.getElement(contentRevKey).getValue();
					latestContentRevId = ContentRetrievalUtil.getLatestdId(contentRevs);
					System.out.println("Latest Content Revision Id is : " + latestContentRevId);
					
					result = contentCache.getElement(contentRevKey).getValue().toString();
					if(result != null){
						//System.out.println("Got the document");
						SAXBuilder sb = new SAXBuilder();
						Document doc = sb.build(new StringReader(result));
						
						List<Element> dictionaryList = (List<Element>) JDOMXPath.selectNodes(doc, "//dictionary/label");
						
						for(int j=0;j<dictionaryList.size();j++){
							Element dictionaryElement = dictionaryList.get(j);
							String labelId = dictionaryElement.getAttributeValue("id")+":"+longLangCode;
							//System.out.println("Id : " + labelId);
							String labelText = dictionaryElement.getValue();
							
							//System.out.println("Text : " + labelText);
							tags.put(labelId, labelText);
						}
					}
					
				}
				
			}/*catch(ProxyException pe){
				pe.printStackTrace();
			} catch (DCAException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JDOMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CacheException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/ catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Iterator tagsItr = tags.keySet().iterator();
		//String prevKey = null;
		List addedKeys = new ArrayList();
		while(tagsItr.hasNext()){
			String key = (String)tagsItr.next().toString().split(":")[0];
			String contentKey= null;
			CacheManager cacheManager = null;
			System.out.println("Key : " + key);
			//System.out.println("Added Keys size : " + addedKeys.size());
			//if(prevKey == null || !prevKey.equals(key)){
			if(!addedKeys.contains(key)){
				System.out.println("Within if and adding the key : " + key);
				addedKeys.add(key);
				DynamicContentAssemblyTextTranslation textTranslation = new DynamicContentAssemblyTextTranslation();
				textTranslation.setTranslationTag(key);
			try{
				cacheManager = CacheManagerFactory.getCacheManagerInstance();
				for(int i=0;i<textTranslationRequest.getLongLanguageCodes().getLongLanguageCodes().size();i++){
					DynamicContentAssemblyTranslation translation = new DynamicContentAssemblyTranslation();
					String longLangCode = textTranslationRequest.getLongLanguageCodes().getLongLanguageCodes().get(i).toString();
					
					Cache contentKeyCache = cacheManager.getCache("ContentKeyCache");
					ContentCLSCacheKey clsCacheKey = new ContentCLSCacheKey("dca_text_translations",getContentServiceLocation(),null,textTranslationRequest.getLongLanguageCodes().getLongLanguageCodes().get(i),null,null,null,null,null,null);
					
					CacheElement contentCacheElement = contentKeyCache.getElement(clsCacheKey);
					
					if(contentCacheElement.getValue() != null)
						contentKey = contentCacheElement.getValue().toString();
					
					if(contentKey != null){ 
						RevisionCacheKey contentRevKey = new RevisionCacheKey(contentKey,getOcmContentUrl());
						
						//Cache contentCache = cacheManager.getCache("ContentCache");
						
						//Get the revision information for content.
						Cache contentRevCache = cacheManager.getCache("RevisionCache");
						
						List<RevisionInfo> contentRevs = (List<RevisionInfo>) contentRevCache.getElement(contentRevKey).getValue();
						latestContentRevId = ContentRetrievalUtil.getLatestdId(contentRevs);
						System.out.println("Latest Content Revision Id is : " + latestContentRevId);
					}	
					//String[] keyArr = keyTags.split(":");
					//String key1 = keyArr[0];
					String transText = tags.get(key+":"+longLangCode);
					
					translation.setLongLanguageCode(longLangCode);
					
					if(transText != null)
						translation.setText(transText);
					else
						translation.setErrorCode("TRANSLATION_TEXT_NOT_FOUND");
					translation.setRevisionNumber(latestContentRevId);
					textTranslation.getTranslations().add(translation);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
				
				response.getTextTranslations().add(textTranslation);
				//prevKey = key;
			}
		}
		
		
		//GetDisclosureTextResponse response = new GetDisclosureTextResponse();
		//response.setResponseList(respList);
		
		
		return response;
	}

}
