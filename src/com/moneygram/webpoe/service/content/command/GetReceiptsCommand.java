package com.moneygram.webpoe.service.content.command;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.axis.utils.ByteArray;



import com.moneygram.DCAService.utils.ContentCLSCacheKey;
import com.moneygram.DCAService.utils.ContentRetrievalUtil;
import com.moneygram.DCAService.utils.DCAException;
import com.moneygram.DCAService.utils.DocPlaceholderSubstUtil;
import com.moneygram.DCAService.utils.RevisionCacheKey;
import com.moneygram.DCAService.utils.RevisionInfo;
import com.moneygram.DCAService.utils.XMLParserUtil;
import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;
import com.moneygram.common.cache.ContextCacheKey;
import com.moneygram.common.cache.ehcache.EHCacheManager;
import com.moneygram.common.dao.DAOException;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.ReadCommand;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.webpoe.dca.service.proxy.ProxyException;
import com.moneygram.webpoe.dca.service.proxy.content.ContentServiceProxy;
import com.moneygram.DCAService.content.ContentContext;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyReceiptRequest;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyReceiptRequestHeader;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyReceiptRequestHeaderList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyReceiptRequestList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyReceiptResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyReceiptResponseList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyRequestHeader;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyRequestHeaderList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyResponseList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyTextResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyToken;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyTokenList;
import com.moneygram.webpoe.service.dca_v1.GetDisclosureTextResponse;
import com.moneygram.webpoe.service.dca_v1.GetReceiptsFormatRequest;
import com.moneygram.webpoe.service.dca_v1.GetReceiptsFormatResponse;



public class GetReceiptsCommand  extends ReadCommand{
	
	private String OCM_CONTENT_URL = null;
	
	
	public String getOcmContentUrl(){
		return OCM_CONTENT_URL;
	}
	
	public void setOcmContentUrl(String ocmContentUrl){
		this.OCM_CONTENT_URL = ocmContentUrl;
	}
	
	private String CONTENT_SERVICE_URL=null;
	
	public String getContentServiceLocation(){
		return CONTENT_SERVICE_URL;
	}
	
	public void setContentServiceLocation(String contentServiceLocation){
		this.CONTENT_SERVICE_URL = contentServiceLocation;
	}
	
	private boolean failOnError = true;
	
	public boolean isFailOnError() {
		return failOnError;
	}

	public void setFailOnError(boolean failOnError) {
		this.failOnError = failOnError;
	}

	private ContentServiceProxy contentServiceProxy;
	
	public ContentServiceProxy getContentServiceProxy() {
		return contentServiceProxy;
	}

	public void setContentServiceProxy(ContentServiceProxy contentServiceProxy) {
		this.contentServiceProxy = contentServiceProxy;
	}
	
	@Override
	public BaseServiceResponseMessage getResponseToReturnError() {
		return new GetReceiptsFormatResponse();
	}

	@Override
	protected boolean isRequestSupported(BaseServiceRequestMessage request)
			throws CommandException {
		return true;
		//return request instanceof GetApplicationsRequest;
	}

	@Override
	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request)
			throws CommandException {
		
		GetReceiptsFormatRequest receiptsRequest = (GetReceiptsFormatRequest) request;
		
		DynamicContentAssemblyReceiptRequestList reqList = receiptsRequest.getReceiptRequestList();
		
		List<DynamicContentAssemblyReceiptRequest>  reqItem = reqList.getReceiptRequests();
		GetReceiptsFormatResponse response = new GetReceiptsFormatResponse();
		DynamicContentAssemblyReceiptResponseList respList = new DynamicContentAssemblyReceiptResponseList();
		
		
		for(int i=0;i<reqItem.size();i++)
		{
			boolean contentExist=true;
			String contentKey= null;
			String languageKey=null;
			List<String> addLangKeys = new ArrayList<String>(); 
			CacheManager cacheManager = null;
			try{
				
				cacheManager = CacheManagerFactory.getCacheManagerInstance();
				Cache contentCache = cacheManager.getCache("ContentKeyCache");
				Cache languageCache = cacheManager.getCache("ContentKeyCache");
				
				if (contentCache == null || languageCache == null) {
		            throw new Exception("Failed to access cache ContentCache");
		        }
				ContentCLSCacheKey clsCacheKey = new ContentCLSCacheKey("dca_receipt_template",getContentServiceLocation(),reqItem.get(i).getCountry(),reqItem.get(i).getLongLanguageCode(),reqItem.get(i).getProvince(),reqItem.get(i).getPoeType(),reqItem.get(i).getReceiptType(),reqItem.get(i).getPosVersion(),null,reqItem.get(i).getPaperFormat());
				
				ContentCLSCacheKey languageCacheKey = new ContentCLSCacheKey("dca_language_dictionary",getContentServiceLocation(),reqItem.get(i).getCountry(),reqItem.get(i).getLongLanguageCode(),reqItem.get(i).getProvince(),reqItem.get(i).getPoeType(),null,reqItem.get(i).getPosVersion(),null,null);
				
				List<ContentCLSCacheKey> addLangCacheKey = new ArrayList<ContentCLSCacheKey>();
				
				if(reqItem.get(i).getAdditionalLanguages() != null){
					List<String> addLangCodes = reqItem.get(i).getAdditionalLanguages().getAdditionalLangCodes();
					for(int j=0;j<addLangCodes.size();j++){
						System.out.println("Additional Language Codes : " + addLangCodes.get(j));
						ContentCLSCacheKey addLangKey = new ContentCLSCacheKey("dca_language_dictionary",getContentServiceLocation(),reqItem.get(i).getCountry(),addLangCodes.get(j),reqItem.get(i).getProvince(),reqItem.get(i).getPoeType(),null,reqItem.get(i).getPosVersion(),null,null);
						addLangCacheKey.add(addLangKey);
					}
				}
				
		        CacheElement contentCacheElement = contentCache.getElement(clsCacheKey);
		        
		        System.out.println("Inside the command 1");
				//Get the receipt text content key
				//contentKey = getContentServiceProxy().getContentKey("dca_receipt_template",reqItem.get(i).getCountry(),reqItem.get(i).getLongLanguageCode(),reqItem.get(i).getProvince(),reqItem.get(i).getPosType(),reqItem.get(i).getReceiptType(),reqItem.get(i).getPosVersion(),null);
				if(contentCacheElement.getValue() != null)
					contentKey = contentCacheElement.getValue().toString();
				else
					contentExist=false;
				
		        CacheElement languageCacheElement = languageCache.getElement(languageCacheKey);
		        
		        if(languageCacheElement.getValue() != null)
		        	languageKey = languageCacheElement.getValue().toString();
		        else
		        	contentExist = false;
		        
		        
		        for(int j=0;j<addLangCacheKey.size();j++){
		        	CacheElement addLangCacheElement = languageCache.getElement(addLangCacheKey.get(j));
		        	addLangKeys.add(addLangCacheElement.getValue().toString());
		        }
		        
		        System.out.println("Size of additional Language Keys : " + addLangKeys.size());
				System.out.println("Content Key received in DCA Service : " + contentKey);
				System.out.println("Language Key received in DCA Service : " + languageKey);
				
			}catch(ProxyException pe){
				System.out.println("Proxy Exception in GetDisclosureTextCommand");
				pe.printStackTrace();
			} catch (CacheException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(contentExist){
					String result = null;
					String languageSubText=null;
					String addLangSubText = null;
					String substitutedResult = null;
					try {
						
						RevisionCacheKey contentRevKey = new RevisionCacheKey(contentKey,getOcmContentUrl());
						RevisionCacheKey languageRevKey = new RevisionCacheKey(languageKey,getOcmContentUrl());
						
						Cache contentCache = cacheManager.getCache("ContentCache");
						Cache languageCache = cacheManager.getCache("ContentCache");
						
						List<RevisionCacheKey> addLanRevKeys = new ArrayList<RevisionCacheKey>();
						for(int j=0;j<addLangKeys.size();j++){
							RevisionCacheKey addLanRevKey = new RevisionCacheKey(addLangKeys.get(j),getOcmContentUrl());
							System.out.println("Adding Additional Key : " + addLanRevKey.getContentkey());
							addLanRevKeys.add(addLanRevKey);
						}
						
						System.out.println("Size of additional Language Revision Keys : "+ addLanRevKeys.size());
						
						//Get the content and language substitution from CMS
						result = contentCache.getElement(contentRevKey).getValue().toString();
						languageSubText = contentCache.getElement(languageRevKey).getValue().toString();
						
						System.out.println("Content got from OCM is : " + result);
						System.out.println("Language got from OCM is : " + languageSubText);
						
						//Carry out the substitution
						HashMap<String,String> languageMap = XMLParserUtil.SingleXmlToHashmap(languageSubText);
					
						//Primary Language Substitution
						substitutedResult = DocPlaceholderSubstUtil.substitute(result, "label:0", languageMap);
						
						//Secondary Language Substitution
						for(int j=0;j<addLanRevKeys.size();j++){
							addLangSubText = contentCache.getElement(addLanRevKeys.get(j)).getValue().toString();
							//Convert to hashmap
							HashMap<String,String> addLanguageMap = XMLParserUtil.SingleXmlToHashmap(addLangSubText);
							substitutedResult = DocPlaceholderSubstUtil.substitute(substitutedResult, "label:"+(j+1), addLanguageMap);
						}
						
						//Primary Language Substitution with label for backward compatibility.
						substitutedResult = DocPlaceholderSubstUtil.substitute(substitutedResult, "label", languageMap);
						
						
						//Token Substitution
						DynamicContentAssemblyTokenList requestTokenList =  reqItem.get(i).getTokenList();
						
						if(requestTokenList != null){
							DynamicContentAssemblyTokenList requestTokens = (DynamicContentAssemblyTokenList)requestTokenList;
							List tokens = requestTokens.getTokens();
							HashMap<String,String> tokenMap = new HashMap<String,String>();
							
							if(tokens != null){
								for(int j=0;j<tokens.size();j++){
									DynamicContentAssemblyToken requestToken = (DynamicContentAssemblyToken) tokens.get(j);
									tokenMap.put(requestToken.getKey(), requestToken.getValue());
								}
								
								substitutedResult = DocPlaceholderSubstUtil.substitute(substitutedResult, "token", tokenMap);
							}
						}
						
						System.out.println("Substituted Text : " + substitutedResult);
						
						//Get the revision information for content and language substitution.
						Cache contentRevCache = cacheManager.getCache("RevisionCache");
						Cache languageRevCache = cacheManager.getCache("RevisionCache");
						
						List<RevisionInfo> contentRevs = (List<RevisionInfo>) contentRevCache.getElement(contentRevKey).getValue();
						System.out.println("size of content revisions : " + contentRevs.size());
						List<RevisionInfo> languageRevs = (List<RevisionInfo>) contentRevCache.getElement(languageRevKey).getValue();
						System.out.println("size of content revisions : " + contentRevs.size());
						String latestContentRevId = ContentRetrievalUtil.getLatestdId(contentRevs);
						String latestLanguageRevId = ContentRetrievalUtil.getLatestdId(languageRevs);
						
						System.out.println("Latest Content Revision Id is : " + latestContentRevId);
						System.out.println("Latest Language Revision Id is : " + latestLanguageRevId);
						int latestContentRevisionId = new Integer(latestContentRevId).intValue();
						int latestLanguageRevisionId = new Integer(latestLanguageRevId).intValue();
						int consumerContentRevisionId =0;
						int consumerLanguageRevisionId=0;
						
						if(reqItem.get(i).getCurrentRevisionNumber() != null)
						{
							StringTokenizer versions = new StringTokenizer(reqItem.get(i).getCurrentRevisionNumber(),".");
							
							while(versions.hasMoreTokens()){
								consumerContentRevisionId = new Integer(versions.nextToken()).intValue();
								consumerLanguageRevisionId = new Integer(versions.nextToken()).intValue();
								System.out.println("Consumer Content Revision Id is : " + consumerContentRevisionId);
								System.out.println("Consumer Language Revision Id is : " + consumerLanguageRevisionId);
							}
						}
						
						
						DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();

						//Compare the revision numbers and 
						
						//Consumer version is earlier than the latest version - return the full content
						/*if( latestContentRevisionId > consumerContentRevisionId){
							
							String md5 = stringToMD5(substitutedResult);
							System.out.println("MD5 Hash is : " + md5);
							resp.setReceiptText(substitutedResult);
							resp.setMD5CheckSum(md5);
							resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
						//Consumer version is later than the latest version - return error code.
						} else if(latestContentRevisionId < consumerContentRevisionId){
							if(latestLanguageRevisionId > consumerLanguageRevisionId){
								String md5 = stringToMD5(substitutedResult);
								System.out.println("MD5 Hash is : " + md5);
								resp.setReceiptText(substitutedResult);
								resp.setMD5CheckSum(md5);
								resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
							}else{
								//DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();
								if(reqItem.get(i).getMD5CheckSum() != null)
									resp.setMD5CheckSum(reqItem.get(i).getMD5CheckSum());
								resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
								resp.setErrorCode("DCA_CONSUMER_VERSION");
							}
							//Consumer version is same as the latest version - check the Checksum hash
						} else if(latestContentRevisionId == consumerContentRevisionId){
							if(latestLanguageRevisionId > consumerLanguageRevisionId){
								String md5 = stringToMD5(substitutedResult);
								System.out.println("MD5 Hash is : " + md5);
								resp.setReceiptText(substitutedResult);
								resp.setMD5CheckSum(md5);
								resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
							} else if(latestLanguageRevisionId < consumerLanguageRevisionId){
								if(reqItem.get(i).getMD5CheckSum() != null)
									resp.setMD5CheckSum(reqItem.get(i).getMD5CheckSum());
								resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
								resp.setErrorCode("DCA_CONSUMER_VERSION");
							}else if(latestLanguageRevisionId == consumerLanguageRevisionId){
								if(reqItem.get(i).getMD5CheckSum() == null){
									//DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();
									resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
									resp.setErrorCode("DCA_NO_CHANGE");
								}else{
									String md5 = stringToMD5(substitutedResult);
									//String md5 = ByteArray.hex(mdEnc.digest());
									if(md5.equals(reqItem.get(i).getMD5CheckSum())){
										//DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();
										resp.setMD5CheckSum(reqItem.get(i).getMD5CheckSum());
										resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
										resp.setErrorCode("DCA_NO_CHANGE");
									}else{
										//DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();
										resp.setReceiptText(substitutedResult);
										resp.setMD5CheckSum(md5);
										resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
									}
								}
							}
						}*/
						
						if(latestContentRevisionId == consumerContentRevisionId && latestLanguageRevisionId == consumerLanguageRevisionId){
							if(reqItem.get(i).getMD5CheckSum() == null){
								//DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();
								resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
								resp.setErrorCode("DCA_NO_CHANGE");
							}else{
								String md5 = stringToMD5(substitutedResult);
								//String md5 = ByteArray.hex(mdEnc.digest());
								if(md5.equals(reqItem.get(i).getMD5CheckSum())){
									//DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();
									resp.setMD5CheckSum(reqItem.get(i).getMD5CheckSum());
									resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
									resp.setErrorCode("DCA_NO_CHANGE");
								}else{
									//DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();
									resp.setReceiptText(substitutedResult);
									resp.setMD5CheckSum(md5);
									resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
								}
							}
						}else{
							String md5 = stringToMD5(substitutedResult);
							System.out.println("MD5 Hash is : " + md5);
							resp.setReceiptText(substitutedResult);
							resp.setMD5CheckSum(md5);
							resp.setRevisionNumber(latestContentRevId+"."+latestLanguageRevId);
						}
						
						if(reqItem.get(i).getLongLanguageCode() != null)
							resp.setLongLanguageCode(reqItem.get(i).getLongLanguageCode());
						if(reqItem.get(i).getProvince() != null)
							resp.setState(reqItem.get(i).getProvince());
						if(reqItem.get(i).getReceiptType() != null)
							resp.setReceiptType(reqItem.get(i).getReceiptType());
						
						
						
						respList.getDCAReceiptResponses().add(resp);
						
					} /*catch (DCAException e) {
					// TODO Auto-generated catch block
						e.printStackTrace();
					}*/catch(IOException ie){
						ie.printStackTrace();
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
			}else{
				DynamicContentAssemblyReceiptResponse resp = new DynamicContentAssemblyReceiptResponse();
				if(reqItem.get(i).getLongLanguageCode() != null)
					resp.setLongLanguageCode(reqItem.get(i).getLongLanguageCode());
				if(reqItem.get(i).getProvince() != null)
					resp.setState(reqItem.get(i).getProvince());
				if(reqItem.get(i).getReceiptType() != null)
					resp.setReceiptType(reqItem.get(i).getReceiptType());
				if(reqItem.get(i).getCurrentRevisionNumber() != null)
					resp.setRevisionNumber(reqItem.get(i).getCurrentRevisionNumber());
				if(reqItem.get(i).getMD5CheckSum() != null)
					resp.setMD5CheckSum(reqItem.get(i).getMD5CheckSum());
				
				resp.setErrorCode("DCA_NOT_FOUND");
				respList.getDCAReceiptResponses().add(resp);
			}
				
				//GetDisclosureTextResponse response = new GetDisclosureTextResponse();
		}
		response.setResponseList(respList);
		return response;
	}
	
	private static String convertedToHex(byte[] data) 
    { 
        StringBuffer buf = new StringBuffer();
        
        for (int i = 0; i < data.length; i++) 
        { 
            int halfOfByte = (data[i] >>> 4) & 0x0F;
            int twoHalfBytes = 0;
            
            do 
            { 
                if ((0 <= halfOfByte) && (halfOfByte <= 9)) 
                {
                    buf.append( (char) ('0' + halfOfByte) );
                }
                
                else 
                {
                    buf.append( (char) ('a' + (halfOfByte - 10)) );
                }

                halfOfByte = data[i] & 0x0F;

            } while(twoHalfBytes++ < 1);
        } 
        return buf.toString();
    } 
	
	private static String stringToMD5(String resultString) throws NoSuchAlgorithmException{
		MessageDigest mdEnc = MessageDigest.getInstance("MD5");
		mdEnc.update(resultString.getBytes(),0, resultString.length());
		byte[] mdEnc5 = new byte[64];
		mdEnc5 = mdEnc.digest();
		String md5 = convertedToHex(mdEnc5);
		return md5;
	}



}
