package com.moneygram.webpoe.service.content.command;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.jaxen.JDOMXPath;

import com.moneygram.DCAService.utils.ContentCLSCacheKey;
import com.moneygram.DCAService.utils.ContentRetrievalUtil;
import com.moneygram.DCAService.utils.DCAException;
import com.moneygram.DCAService.utils.RevisionCacheKey;
import com.moneygram.DCAService.utils.RevisionInfo;
import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;
import com.moneygram.common.dao.DAOException;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.ReadCommand;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.webpoe.dca.service.proxy.ProxyException;
import com.moneygram.webpoe.dca.service.proxy.content.ContentServiceProxy;
import com.moneygram.webpoe.service.content.dao.Xml;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyDisclosureRequest;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyDisclosureRequestList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyResponseList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorInfoRequest;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorInfoRequestList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorInfoResponse;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorInfoResponseList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorName;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyStateRegulatorNameList;
import com.moneygram.webpoe.service.dca_v1.DynamicContentAssemblyTextResponse;
import com.moneygram.webpoe.service.dca_v1.GetDisclosureTextRequest;
import com.moneygram.webpoe.service.dca_v1.GetDisclosureTextResponse;
import com.moneygram.webpoe.service.dca_v1.GetStateRegulatorInfoRequest;
import com.moneygram.webpoe.service.dca_v1.GetStateRegulatorInfoResponse;
import com.moneygram.webpoe.service.dca_v1.LongLanguageCode;



public class GetStateRegulatorInfoCommand  extends ReadCommand{
	
	private String OCM_CONTENT_URL = null;
	
	
	public String getOcmContentUrl(){
		return OCM_CONTENT_URL;
	}
	
	public void setOcmContentUrl(String ocmContentUrl){
		this.OCM_CONTENT_URL = ocmContentUrl;
	}
	
	private String CONTENT_SERVICE_URL=null;
	
	public String getContentServiceLocation(){
		return CONTENT_SERVICE_URL;
	}
	
	public void setContentServiceLocation(String contentServiceLocation){
		this.CONTENT_SERVICE_URL = contentServiceLocation;
	}
	
	private ContentServiceProxy contentServiceProxy;
	
	public ContentServiceProxy getContentServiceProxy() {
		return contentServiceProxy;
	}

	public void setContentServiceProxy(ContentServiceProxy contentServiceProxy) {
		this.contentServiceProxy = contentServiceProxy;
	}
	
	@Override
	public BaseServiceResponseMessage getResponseToReturnError() {
		return new GetStateRegulatorInfoResponse();
	}

	@Override
	protected boolean isRequestSupported(BaseServiceRequestMessage request)
			throws CommandException {
		return true;
		//return request instanceof GetApplicationsRequest;
	}

	@Override
	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request)
			throws CommandException {
		
		GetStateRegulatorInfoRequest stateRegulatorInfoRequest = (GetStateRegulatorInfoRequest) request;;
		DynamicContentAssemblyStateRegulatorInfoRequestList reqList = stateRegulatorInfoRequest.getStateRegulatorInfoRequestList();
		List<DynamicContentAssemblyStateRegulatorInfoRequest>  reqItem = reqList.getStateRegulatorInfoRequests();
		
		DynamicContentAssemblyStateRegulatorInfoResponseList respList = new DynamicContentAssemblyStateRegulatorInfoResponseList();
		DynamicContentAssemblyStateRegulatorInfoResponse resp = new DynamicContentAssemblyStateRegulatorInfoResponse();
		

		DynamicContentAssemblyStateRegulatorNameList nameList = new DynamicContentAssemblyStateRegulatorNameList();
		DynamicContentAssemblyStateRegulatorName names = new DynamicContentAssemblyStateRegulatorName();
		GetStateRegulatorInfoResponse response = new GetStateRegulatorInfoResponse();
		
		//Content Key Service Call
		String contentKey= null;
		String languageKey=null;
		boolean contentExist=true;
		String result = null;
		String phone = null;
		String url = null;
		CacheManager cacheManager = null;
		for(int i=0;i<reqItem.size();i++)
		{
			
			try{
				cacheManager = CacheManagerFactory.getCacheManagerInstance();
				Cache contentKeyCache = cacheManager.getCache("ContentKeyCache");
				
				if (contentKeyCache == null) {
		            throw new Exception("Failed to access cache ContentCache");
		        }
				
				
				
				//contentServiceProxy = new ContentServiceProxyImpl();
				resp = new DynamicContentAssemblyStateRegulatorInfoResponse();
				System.out.println("Inside the command");
				String state = reqItem.get(i).getState();
				resp.setErrorCode("none");
				resp.setRevisionNumber("1.0");
				resp.setState(state);
				LongLanguageCode languageCodes = reqItem.get(i).getLongLanguageCodes();
				List<String> languages = languageCodes.getLongLanguageCodes();
				nameList = new DynamicContentAssemblyStateRegulatorNameList();
				for(int j=0;j<languages.size();j++){
					names = new DynamicContentAssemblyStateRegulatorName();
					ContentCLSCacheKey clsCacheKey = new ContentCLSCacheKey("dca_state_regulator",getContentServiceLocation(),null,languages.get(j),reqItem.get(i).getState(),null,null,null,null,null);
					CacheElement contentCacheElement = contentKeyCache.getElement(clsCacheKey);
					//contentKey = getContentServiceProxy().getContentKey("dca_state_regulator",null,languages.get(j),reqItem.get(i).getState(),null,null,null,null);
					 if(contentCacheElement.getValue() != null)
							contentKey = contentCacheElement.getValue().toString();
						else
							contentExist=false;
					String language = languages.get(j);
					System.out.println("Content Key received in DCA Service : " + contentKey);
					//result = ContentRetrievalUtil.getContent( contentKey, ocmUrl, 11000 );
					if(contentKey != null){
						RevisionCacheKey contentRevKey = new RevisionCacheKey(contentKey,getOcmContentUrl());
						
						Cache contentCache = cacheManager.getCache("ContentCache");
						
						
						result = contentCache.getElement(contentRevKey).getValue().toString();
						//result = ContentRetrievalUtil.getContent( contentKey, ocmUrl, 11000 );
						SAXBuilder sb = new SAXBuilder();
						Document doc = sb.build(new StringReader(result));
						System.out.println("Got the doc");
						Element node = (Element)JDOMXPath.selectSingleNode(doc, "//stateregulatorinfolist/stateregulatorinfo[@state=\"" + state + "\"]");
						Element nameListReq = (Element) JDOMXPath.selectSingleNode(doc, "//stateregulatorinfolist/stateregulatorinfo[@state=\"" + state + "\"]/namelist/name[@language=\"" + language + "\"]");
						
						if(nameListReq != null){
							String nameXML = nameListReq.getText();
							
							phone = node.getChildText("phone");
							url = node.getChildText("url"); 
							names.setName(nameXML);
							names.setLongLanguageCode(language);
							
							System.out.println("Name : " + nameXML);
							System.out.println("Phone : " + phone);
							System.out.println("URL : " + url);
							nameList.getDCARegulatorNames().add(names);
						}
					}
					resp.setNames(nameList);
				}
				
			}catch(ProxyException pe){
				System.out.println("Proxy Exception in GetDisclosureTextCommand");
				pe.printStackTrace();
			}catch (DCAException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JDOMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CacheException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			resp.setPhone(phone);
			resp.setUrl(url);
			respList.getDCAStateRegulatoreInfoResponses().add(resp);
		}
		
		//GetDisclosureTextResponse response = new GetDisclosureTextResponse();
		response.setResponseList(respList);
		
		return response;
	}

}
