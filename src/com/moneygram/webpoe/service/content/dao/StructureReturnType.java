package com.moneygram.webpoe.service.content.dao;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.sql.STRUCT;

import org.springframework.jdbc.core.SqlReturnType;

/**
 * 
 * Structure Return Type.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>PartnerServiceWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2011/08/22 19:14:02 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class StructureReturnType implements SqlReturnType {
	private static final Logger logger = Logger.getLogger(StructureReturnType.class.getName());
	
	/**
	 * 
	 * @see org.springframework.jdbc.core.SqlReturnType#getTypeValue(java.sql.CallableStatement, int, int, java.lang.String)
	 */
	public Object getTypeValue(CallableStatement cs, int colIndx,
			int sqlType, String typeName) throws SQLException {
		
		STRUCT struct = (STRUCT) cs.getObject(colIndx);
		Object[] attributes = (struct == null ? null : struct.getAttributes());

		if (logger.isLoggable(Level.FINEST)) {
			logger.finest("getTypeValue: attributes="+ attributes +" colIndx="+ colIndx +" sqlType="+ sqlType +" typeName="+ typeName);
		}
		
		return attributes;
	}
}
