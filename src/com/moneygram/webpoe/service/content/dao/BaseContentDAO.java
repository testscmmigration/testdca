/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.webpoe.service.content.dao;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.nativejdbc.NativeJdbcExtractor;

import com.moneygram.common.dao.BaseDAO;

/**
 * 
 * Base Content Service DAO.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>ContentServiceWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2012/02/07 18:46:22 $ </td><tr><td>
 * @author   </td><td>$Author: ve64 $ </td>
 *</table>
 *</div>
 */
public abstract class BaseContentDAO extends BaseDAO {
	private static final Logger logger = Logger.getLogger(BaseContentDAO.class.getName());
	
	protected static final String WEB_POE_PACKAGE_PREFIX = "pkg_web_poe_dashboard.";

	protected void initDao() throws Exception {
		super.initDao();
		/*NativeJdbcExtractor extractor = getJdbcTemplate().getNativeJdbcExtractor();
		if (logger.isLoggable(Level.FINE)) {
			logger.fine("initDao: extractor="+ extractor);
		}*/
	}
	
	/**
     * Sets the application specific call type input parameter.
     * @param parameters
     */
    protected void addCallTypeParameter(MapSqlParameterSource parameters) {
        parameters.addValue("iv_call_type_code", "webpoe");
    }

    /**
     * Sets the application specific db user input parameter.
     * @param parameters
     */
    protected void addUserParameter(MapSqlParameterSource parameters) {
        parameters.addValue("iv_user_id", "webpoe");
    }
}
