package com.moneygram.webpoe.service.content.dao;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.sql.ARRAY;
import oracle.sql.STRUCT;

import org.springframework.jdbc.core.SqlReturnType;

/**
 * 
 * Array Of Structures Return Type.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>PartnerServiceWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2011/06/23 21:49:43 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ArrayOfStructuresReturnType implements SqlReturnType {
	private static final Logger logger = Logger.getLogger(ArrayOfStructuresReturnType.class.getName());
	
	/**
	 * 
	 * @see org.springframework.jdbc.core.SqlReturnType#getTypeValue(java.sql.CallableStatement, int, int, java.lang.String)
	 */
	public Object getTypeValue(CallableStatement cs, int colIndx,
			int sqlType, String typeName) throws SQLException {
		ARRAY array = (ARRAY) cs.getObject(colIndx);
		
		List<Object[]> list = toListOfObjectArrays(array);
		if (logger.isLoggable(Level.FINEST)) {
			logger.finest("getTypeValue: list="+ list +" colIndx="+ colIndx +" sqlType="+ sqlType +" typeName="+ typeName);
		}
		return list;
	}

	public static List<Object[]> toListOfObjectArrays(ARRAY array) throws SQLException {
		Object[] structs = (Object[]) array.getArray();
		List<Object[]> list = toListOfObjectArrays(structs);
		return list;
	}

	public static List<Object[]> toListOfObjectArrays(Object[] structs) throws SQLException {
		List<Object[]> list = new ArrayList<Object[]>();
		for (Object struct : structs) {
			Object[] attributes = ((STRUCT)struct).getAttributes();
			list.add(attributes);
		}
		return list;
	}

}

