package com.moneygram.webpoe.service.content.dao;

import java.sql.Types;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.moneygram.common.dao.DAOException;

public class ContentDAO extends BaseContentDAO{
	
	private static final Logger logger = Logger.getLogger(ContentDAO.class.getName());
	
	private static final String GET_CONTENT_KEY = WEB_POE_PACKAGE_PREFIX +"poe_get_cntnt_for_appln";
	
	public String getContentKey(String contentItemName, String languageCode, String bsnsCode, String countryCode) throws DAOException{
		SimpleJdbcCall procedure =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(GET_CONTENT_KEY)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                    	new SqlParameter("iv_user_id", Types.VARCHAR),
                    	new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_bsns_area_code", Types.VARCHAR),
                        new SqlParameter("iv_iso_lang_short", Types.VARCHAR),
                        new SqlParameter("iv_iso_cntry_abbr_code", Types.VARCHAR),
                        new SqlParameter("iv_src_item_cntnr_name", Types.VARCHAR),
                        new SqlOutParameter("ov_appln_cntnt_cv_type", Types.VARCHAR)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        addUserParameter(parameters);
        addCallTypeParameter(parameters);
        parameters.addValue("iv_bsns_area_code", bsnsCode);
        parameters.addValue("iv_iso_lang_short", languageCode);
        parameters.addValue("iv_iso_cntry_abbr_code", countryCode);
        parameters.addValue("iv_src_item_cntnr_name", contentItemName);
        

        if (logger.isLoggable(Level.FINE)) {
			logger.fine("getUser: calling procedure="+ GET_CONTENT_KEY +" with "+ parameters.getValues().size() +" input parameters for content item name ="+ contentItemName + ", language code = " + languageCode + ", business code = " + bsnsCode);
		}
        
        String contentKey = null;
        
        try {
        	@SuppressWarnings("rawtypes")
			Map result = procedure.execute(parameters);
        	contentKey = (String)result.get("ov_appln_cntnt_cv_type");

        } catch (Exception e) {
            throw new DAOException("Failed to obtain the content key for content item name ="+ contentItemName + ", language code = " + languageCode + ", bsns code = " + bsnsCode, e);
        }        
		return contentKey;
		//return "WPOE_WM_HELP_SND_CA_US_EN";
	}
}	
