package com.moneygram.webpoe.service.content.dao;

import java.io.Serializable;

public class StateInfo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String phone;
	private String url;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	

}
