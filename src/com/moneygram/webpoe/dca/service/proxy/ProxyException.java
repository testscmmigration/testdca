/*
 * Created on Mar 28, 2011
 *
 */
package com.moneygram.webpoe.dca.service.proxy;

/**
 * 
 * Proxy Exception.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>PortalPlatformWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2011/03/30 21:38:18 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ProxyException extends Exception {
    private static final long serialVersionUID = -6515022454705095520L;

    public ProxyException() {
        super();
        
    }

    public ProxyException(String message, Throwable cause) {
        super(message, cause);
        
    }

    public ProxyException(String message) {
        super(message);
        
    }

    public ProxyException(Throwable cause) {
        super(cause);
        
    }

}
