package com.moneygram.webpoe.dca.service.proxy.content;

import com.moneygram.webpoe.dca.service.proxy.ProxyException;

public interface ContentServiceProxy {
	
	/**
	 * Returns the list of the Widget instances. 
	 * @return
	 * @throws ProxyException
	 */
	public String getContentKey(String contentItemName,String contentServiceLocation,String country,String locale,String province,String posType,String receiptType,String posVersion,String disclosureTag,String paperFormat) throws ProxyException;
	
}
