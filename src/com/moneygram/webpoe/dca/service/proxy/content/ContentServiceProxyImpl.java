package com.moneygram.webpoe.dca.service.proxy.content;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.namespace.QName;

import org.springframework.stereotype.Component;

import com.moneygram.webpoe.dca.service.proxy.BaseProxy;
import com.moneygram.webpoe.dca.service.proxy.ProxyException;
import com.moneygram.webpoe.dca.util.ResourceConfig;
import com.moneygram.webpoe.dca.util.WebPOEResourceConfig;
import com.moneygram.webpoe.service.content_v1.Attributes;
import com.moneygram.webpoe.service.content_v1.AttributesMap;
import com.moneygram.webpoe.service.content_v1.ContentV1;
import com.moneygram.webpoe.service.content_v1.GetContentKeyRequest;
import com.moneygram.webpoe.service.content_v1.GetContentKeyResponse;
import com.moneygram.webpoe.service.content_v1.WebPOEContentServicePortType;
import com.moneygram.webpoe.service.dca_v1.GetDisclosureTextRequest;
import com.moneygram.common.util.ApplicationContextProvider;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;



public class ContentServiceProxyImpl extends BaseProxy implements ContentServiceProxy {
	
	private static final Logger logger = Logger
			.getLogger(ContentServiceProxyImpl.class.getName());
	
	private String CONTENT_SERVICE_URL=null;
	
	public String getContentServiceLocation(){
		return CONTENT_SERVICE_URL;
	}
	
	public void setContentServiceLocation(String contentServiceLocation){
		this.CONTENT_SERVICE_URL = contentServiceLocation;
	}
	
	public String getContentKey(String contentItemName,String contentServiceLocation,String country,String longLanguageCode,String province,String posType,String receiptType,String posVersion,String disclosureTag,String paperFormat) throws ProxyException {
		System.out.println("Inside the proxy and calling service");
		WebPOEContentServicePortType service = getService(contentServiceLocation);
		String contentKey=null;
		System.out.println("Came back from getService method");
		GetContentKeyRequest request = new GetContentKeyRequest();
		
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction("GetContentKey");
		
		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);

		request.setHeader(header);
		
		request.setContentItemName(contentItemName);
		//request.setApplicationCode("DCA");
		
		AttributesMap attrMap = new AttributesMap();
		
		
		
		//attr.setKey("language");
		//attr.setValue("en");
		if(country != null){
			Attributes attr = new Attributes();
			attr.setKey("country");
			attr.setValue(country);
			System.out.println("Adding country");
			attrMap.getAttribute().add(attr);
		}
		
		if(longLanguageCode != null){
			Attributes attr1 = new Attributes();
			attr1.setKey("longlanguagecode");
			attr1.setValue(longLanguageCode);
			System.out.println("Adding locale");
			attrMap.getAttribute().add(attr1);
		}
		
		if(province != null){
			Attributes attr2 = new Attributes();
			attr2.setKey("state");
			attr2.setValue(province);
			System.out.println("Adding province");
			attrMap.getAttribute().add(attr2);
		}
		
		if(posType != null){
			Attributes attr3 = new Attributes();
			attr3.setKey("poetype");
			attr3.setValue(posType);
			System.out.println("Adding postype");
			attrMap.getAttribute().add(attr3);
		}
		
		if(receiptType != null){
			Attributes attr4 = new Attributes();
			attr4.setKey("receipttype");
			attr4.setValue(receiptType);
			System.out.println("Adding receipttype");
			attrMap.getAttribute().add(attr4);
		}
		
		if(posVersion != null){
			Attributes attr5 = new Attributes();
			attr5.setKey("posversion");
			attr5.setValue(posVersion);
			System.out.println("Adding posversion");
			attrMap.getAttribute().add(attr5);
		}
		
		if(disclosureTag != null){
			Attributes attr6 = new Attributes();
			attr6.setKey("disclosuretag");
			attr6.setValue(disclosureTag);
			System.out.println("Adding disclosure tag");
			attrMap.getAttribute().add(attr6);
		}
		
		if(paperFormat != null){
			Attributes attr7 = new Attributes();
			attr7.setKey("paperformat");
			attr7.setValue(paperFormat);
			System.out.println("Adding Paper Format");
			attrMap.getAttribute().add(attr7);
		}
		
		
		
		request.setAttributesMap(attrMap);
		
		System.out.println("Set the request parametrrts with attrMap size " + attrMap.getAttribute().size());
		
		GetContentKeyResponse response = new GetContentKeyResponse();
		
		try{
			System.out.println("Actually calling the service");
			response = service.getContentKey(request);
			System.out.println("And the content key got is : " + response.getOcmKey());
			contentKey = response.getOcmKey();
		}catch(Exception e){
			logger.info("Error in ContentServiceProxyImpl : ");
			e.printStackTrace();
		}
		System.out.println("Returning the content key : " + contentKey);
		return contentKey;
	}

	private WebPOEContentServicePortType getService(String contentServiceLocation) throws ProxyException{
		URL url = null;
		String contentServiceUrl="";
		try{
			System.out.println("Inside getService method and setting URL");
			//contentServiceUrl = "http://d3wsintsvcs.qacorp.moneygram.com/contentService/content_v1?wsdl";
			 
			//contentServiceUrl = "http://localhost:8080/contentService/content_v1?wsdl";
			URL baseUrl;
			baseUrl = com.moneygram.webpoe.service.content_v1.ContentV1.class.getResource(".");
			url = new URL(baseUrl,contentServiceLocation);
			System.out.println("Set the URL to : " + url.toString());
		}catch (MalformedURLException e) {
            logger.severe("Failed to create URL for the wsdl Location: "+contentServiceUrl);
            logger.severe(e.getMessage());
            throw new ProxyException("Failed to create URL for the wsdl Location: "+contentServiceUrl,e);
        }
		System.out.println("Calling the ContentV1 cons with URL : " + url);
		ContentV1 service = new ContentV1(url,new QName("http://moneygram.com/webpoe/service/content_v1", "content_v1"));
		System.out.println("Returning from getService method");
		return service.getWebPOEContentServicePort();
	}
	
	
}
