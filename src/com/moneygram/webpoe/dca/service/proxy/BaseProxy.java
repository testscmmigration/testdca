/*
 * Created on Apr 26, 2011
 *
 */
package com.moneygram.webpoe.dca.service.proxy;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;

import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;
import com.moneygram.webpoe.dca.util.ResourceConfig;
import com.moneygram.webpoe.service.BaseService;
import com.moneygram.webpoe.service.content_v1.WebPOEContentServicePortType;

/**
 * 
 * Abstract Base Proxy.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>PortalPlatformWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2011/07/21 20:42:27 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */

public abstract class BaseProxy extends BaseService {

    protected ResourceConfig resourceConfigDCA;

    /**
	 * Returns a Cache instance using the cache manager.
	 * @param cacheName
	 * @return Cache instance 
	 * @throws ProxyException
	 */
	protected Cache getCache(String cacheName) throws ProxyException {
		try {
			CacheManager cacheManager = CacheManagerFactory.getCacheManagerInstance();
	        Cache cache = cacheManager.getCache(cacheName);
	        if (cache == null) {
	            throw new ProxyException("Failed to access cache="+ cacheName);
	        }
	        return cache;
		} catch (CacheException e) {
            throw new ProxyException("Failed to access cache="+ cacheName, e);
		}
	}

	/**
	 * Returns cache element from the cache.
	 * @param cacheName
	 * @param key
	 * @return cache element 
	 * @throws ProxyException
	 */
	protected Serializable getCacheElement(String cacheName, Serializable key) throws ProxyException {
		Cache cache = getCache(cacheName);
        try {
			CacheElement cacheElement = cache.getElement(key);
			return (cacheElement == null ? null : cacheElement.getValue());
		} catch (CacheException e) {
			throw new ProxyException(
					"Failed to retrieve cache element from cache="+ cacheName +" for key="+ key, e);
		}
	}

	public ResourceConfig getResourceConfigDCA() {
		return resourceConfigDCA;
	}

	public void setResourceConfigDCA(ResourceConfig resourceConfigDCA) {
		this.resourceConfigDCA = resourceConfigDCA;
	}

}
