package com.moneygram.webpoe.dca.util;

import org.springframework.stereotype.Component;
import com.moneygram.webpoe.dca.util.ResourceConfig;


public class WebPOEResourceConfig extends ResourceConfig{

	public static final String CONTENT_SERVICE_URL = "CONTENT_SERVICE_URL";
	public static final String CONTENT_SERVICE_NAMESPACE_URI = "CONTENT_SERVICE_NAMESPACE_URI";
	public static final String OCM_CONTENT_URL = "OCM_CONTENT_URL";
	
	
	public String getContentServiceLocation(){
		return getAttributeValue(CONTENT_SERVICE_URL);
	}
	
	public String getOcmContentUrl(){
		return getAttributeValue(OCM_CONTENT_URL);
	}
	
}
	
