/*
 * Created on Jul 22, 2011
 *
 */
package com.moneygram.webpoe.dca.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import com.moneygram.ree.lib.Config;

/**
 * 
 * Container based Jax Ws Port Proxy Factory Bean.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>PortalPlatformWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2011/07/22 21:50:02 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ContainerJaxWsPortProxyFactoryBean extends JaxWsPortProxyFactoryBean {
	private static final Logger logger = Logger.getLogger(ContainerJaxWsPortProxyFactoryBean.class.getName());
	
	/**
	 * Default jax-ws timeout property (WAS / Axis 2) in seconds
	 * 
	 * com.ibm.wsspi.webservices.Constants.CONNECTION_TIMEOUT_PROPERTY = "connection_timeout"
	 * com.ibm.wsspi.webservices.Constants.WRITE_TIMEOUT_PROPERTY = "write_timeout"
	 * com.ibm.wsspi.webservices.Constants.RESPONSE_TIMEOUT_PROPERTY = "timeout"
	 * com.ibm.wsspi.webservices.Constants.READ_TIMEOUT_PROPERTY = "read_timeout"
	 *
	 * These are the values for SUN's implementation - in milliseconds
	 * 
	 * com.sun.xml.ws.request.timeout
	 * com.sun.xml.ws.connect.timeout
	 */
	private static final String DEFAULT_TIMEOUT_PROPERTY = "timeout";

	/**
	 * Default jax-ws timeout value in seconds
	 * 
	 */
	private static final String DEFAULT_TIMEOUT_VALUE = "15";
	
	/**
	 * instance of the resource config.
	 */
	private Config config = null;
	
	private String wsdlDocumentUrlProperty = null;

	private String timeoutProperty = null;

	private String timeoutCustomPropertyValue = null;

	public void afterPropertiesSet() {
        super.afterPropertiesSet();
        
        String urlValue = getWsdlDocumentUrlPropertyValue();
        
        logger.info("afterPropertiesSet: using WsdlDocumentUrl="+ urlValue);

        try {
			URL url = new URL(urlValue);
			setWsdlDocumentUrl(url);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("Incorrect Wsdl Document Url specified="+ urlValue, e);
		}
		
		Map<String, Object> map = getCustomProperties();
		logger.info("afterPropertiesSet: CustomProperties="+ map);
		
		String timeoutProperty = getTimeoutCustomPropertyValueWithDefault();
		String timeoutValue = getTimeoutPropertyValue();

		Object propertyValue = timeoutValue;

		if (timeoutProperty.startsWith("com.sun.xml.ws")) {
			//convert to integer and milliseconds
			int tm = Integer.valueOf(timeoutValue).intValue();
			propertyValue = Integer.valueOf(tm * 1000);
		}
		
		logger.info("afterPropertiesSet: setting "+ timeoutProperty +"="+ propertyValue +" in custom properties="+ map);
		
		map.put(timeoutProperty, propertyValue);
    }

    protected String getWsdlDocumentUrlPropertyValue() {
    	String key = getWsdlDocumentUrlProperty();
    	if (key == null) {
			throw new IllegalArgumentException("wsdlDocumentUrlProperty is required");
		}
    	
		return getConfigPropertyValue(key, null);
	}

    protected String getTimeoutPropertyValue() {
    	String key = getTimeoutProperty();
    	
    	if (key == null) {
			throw new IllegalArgumentException("timeoutProperty is required");
		}
		return getConfigPropertyValue(key, DEFAULT_TIMEOUT_VALUE);
	}

	protected String getTimeoutCustomPropertyValueWithDefault() {
    	String value = getTimeoutCustomPropertyValue();
    	if (value == null) {
			value = DEFAULT_TIMEOUT_PROPERTY;
		}
		return value;
	}

    private String getConfigPropertyValue(String key, String defaultValue) {
    	Properties properties = getConfigProperties();
    	
    	String value = properties.getProperty(key);

    	if (value == null) {
    		if (defaultValue == null) {
    			throw new IllegalArgumentException("config does not contain property="+ key);
			} else {
				logger.info("getConfigPropertyValue: config does not contain property="+ key +". Using default value="+ defaultValue);
				value = defaultValue;
			}
		}
    	
		return value;
    }


    private Properties getConfigProperties() {
    	if (getConfig() == null) {
			throw new IllegalArgumentException("config is required");
		}
    	if (config.getProperties() == null) {
			throw new IllegalArgumentException("config is empty");
		}
    	
    	return config.getProperties();
    }
    
    public String getTimeoutProperty() {
		return timeoutProperty;
	}

	public void setTimeoutProperty(String timeoutProperty) {
		this.timeoutProperty = timeoutProperty;
	}

	public String getTimeoutCustomPropertyValue() {
		return timeoutCustomPropertyValue;
	}

	public void setTimeoutCustomPropertyValue(String timeoutCustomPropertyValue) {
		this.timeoutCustomPropertyValue = timeoutCustomPropertyValue;
	}

	/**
     * @return Returns the config.
     */
    public Config getConfig() {
        return config;
    }

    /**
     * @param config The config to set.
     */
    public void setConfig(Config config) {
        this.config = config;
    }

    public String getWsdlDocumentUrlProperty() {
		return wsdlDocumentUrlProperty;
	}

	public void setWsdlDocumentUrlProperty(String wsdlDocumentUrlProperty) {
		this.wsdlDocumentUrlProperty = wsdlDocumentUrlProperty;
	}

}
