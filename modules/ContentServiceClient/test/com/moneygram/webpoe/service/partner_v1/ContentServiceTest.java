/*
 * Created on May 18, 2011
 *
 */
package com.moneygram.webpoe.service.partner_v1;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceRef;
import junit.framework.TestCase;

import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.webpoe.service.content_v1.ContentV1;
import com.moneygram.webpoe.service.content_v1.GetContentKeyRequest;
import com.moneygram.webpoe.service.content_v1.GetContentKeyResponse;
import com.moneygram.webpoe.service.content_v1.WebPOEContentServicePortType;

/**
 * 
 * Content Service Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>ContentServiceClient</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2012/02/02 18:27:29 $ </td><tr><td>
 * @author   </td><td>$Author: ve64 $ </td>
 *</table>
 *</div>
 */
public class ContentServiceTest extends TestCase {
	private static final Logger logger = Logger
			.getLogger(ContentServiceTest.class.getName());
	
	private static final String BASE_URL = "http://localhost:8080";
//	private static final String BASE_URL = "http://localhost:9086";
	//private static final String BASE_URL = "http://d2wsintsvcs.qacorp.moneygram.com";
	private static final String ENDPOINT_URL = BASE_URL +"/contentService/content_v1";
	
	@WebServiceRef(wsdlLocation="http://localhost:8080/contentService/content_v1?wsdl")
	private static ContentV1 service = null;
	
	

	public void testGetContentKey() throws Exception {
		GetContentKeyRequest request = new GetContentKeyRequest();
		setupRequest(request, "GetContentKey");
	
		String contentItemName = "wm_help";
		String languageCode = "en";
		String applicationName = "core";
		
		request.setContentItemName(contentItemName);
		request.setLanguageCode(languageCode);
		request.setApplicationName(applicationName);
		
		ContentV1 service = getServiceReference();

		WebPOEContentServicePortType port = service.getWebPOEContentServicePort();
		
		try {
			GetContentKeyResponse response = port.getContentKey(request);
	
			assertNotNull("expected not null response", response);
			
			//retrieve the contentKey
			logger.log(Level.FINE,response.getOcmKey());
			
		} catch (Error e) {
			logger.log(Level.SEVERE, "Failed to process the request: "+ getErrorMessage(e));
			fail("Failed to process the request");
		}

	}

	private String getErrorMessage(Error e) {
		StringBuffer buffer = new StringBuffer();
		
		/*ServiceError se = e.getFaultInfo();
		
		if (se == null) {*/
			buffer.append(e.getMessage());
		/*} else {
			buffer.append("code=").append(se.getErrorCode()).append(": ").append(se.getErrorMessage());
			buffer.append(se.getErrorStackTrace());
		}*/

		return buffer.toString();
	}

	private static ContentV1 getServiceReference() throws Exception {
		if (service == null) {
			synchronized (ContentServiceTest.class) {
				if (service == null) {
					QName serviceName = new QName("http://moneygram.com/webpoe/service/content_v1", "content_v1");
					URL endpoint = new URL(ENDPOINT_URL);

					logger.info("getServiceReference: Service Reference has not been injected, using constructor with endpoint="+ endpoint);
					
					service = new ContentV1(endpoint, serviceName);
				}
			}
		}
		return service;
	}
	
	private BaseServiceRequestMessage setupRequest(BaseServiceRequestMessage request, String action) {
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(action);
		
		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);

		request.setHeader(header );
		return request;
	}
}
