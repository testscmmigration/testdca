
package com.moneygram.common_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.webpoe.service.content_v1.GetApplicationsResponse;
import com.moneygram.webpoe.service.content_v1.GetContentKeyResponse;
import com.moneygram.webpoe.service.content_v1.GetLabelResponse;
import com.moneygram.webpoe.service.content_v1.GetSupportedLanguagesResponse;


/**
 * <p>Java class for BaseServiceResponseMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseServiceResponseMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://moneygram.com/common_v1}Header"/>
 *         &lt;element name="error" type="{http://moneygram.com/common_v1}ServiceError" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseServiceResponseMessage", propOrder = {
    "header",
    "error"
})
@XmlSeeAlso({
    GetSupportedLanguagesResponse.class,
    GetContentKeyResponse.class,
    GetLabelResponse.class,
    GetApplicationsResponse.class
})
public class BaseServiceResponseMessage {

    @XmlElement(required = true)
    protected Header header;
    protected ServiceError error;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceError }
     *     
     */
    public ServiceError getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceError }
     *     
     */
    public void setError(ServiceError value) {
        this.error = value;
    }

}
