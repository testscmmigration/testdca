
package com.moneygram.webpoe.service.content_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.moneygram.common_v1.BaseServiceResponseMessage;


/**
 * <p>Java class for GetContentKeyResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetContentKeyResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceResponseMessage">
 *       &lt;sequence>
 *         &lt;element name="ocmKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetContentKeyResponse", propOrder = {
    "ocmKey"
})
public class GetContentKeyResponse
    extends BaseServiceResponseMessage
{

    @XmlElement(required = true)
    protected String ocmKey;

    /**
     * Gets the value of the ocmKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOcmKey() {
        return ocmKey;
    }

    /**
     * Sets the value of the ocmKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOcmKey(String value) {
        this.ocmKey = value;
    }

}
