
package com.moneygram.webpoe.service.content_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.moneygram.common_v1.BaseServiceResponseMessage;


/**
 * <p>Java class for GetSupportedLanguagesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSupportedLanguagesResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceResponseMessage">
 *       &lt;sequence>
 *         &lt;element name="languages" type="{http://moneygram.com/webpoe/service/content_v1}LanguageList"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSupportedLanguagesResponse", propOrder = {
    "languages"
})
public class GetSupportedLanguagesResponse
    extends BaseServiceResponseMessage
{

    @XmlElement(required = true)
    protected LanguageList languages;

    /**
     * Gets the value of the languages property.
     * 
     * @return
     *     possible object is
     *     {@link LanguageList }
     *     
     */
    public LanguageList getLanguages() {
        return languages;
    }

    /**
     * Sets the value of the languages property.
     * 
     * @param value
     *     allowed object is
     *     {@link LanguageList }
     *     
     */
    public void setLanguages(LanguageList value) {
        this.languages = value;
    }

}
