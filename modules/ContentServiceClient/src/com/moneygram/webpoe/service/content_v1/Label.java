
package com.moneygram.webpoe.service.content_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Label complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Label">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bsnsAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="appCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="localText" type="{http://moneygram.com/webpoe/service/content_v1}LocaleText" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tags" type="{http://moneygram.com/webpoe/service/content_v1}LabelTagsList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Label", propOrder = {
    "bsnsAreaCode",
    "appCode",
    "key",
    "localText",
    "tags"
})
public class Label {

    @XmlElement(required = true)
    protected String bsnsAreaCode;
    @XmlElement(required = true)
    protected String appCode;
    @XmlElement(required = true)
    protected String key;
    protected List<LocaleText> localText;
    @XmlElement(required = true)
    protected LabelTagsList tags;

    /**
     * Gets the value of the bsnsAreaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBsnsAreaCode() {
        return bsnsAreaCode;
    }

    /**
     * Sets the value of the bsnsAreaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBsnsAreaCode(String value) {
        this.bsnsAreaCode = value;
    }

    /**
     * Gets the value of the appCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppCode() {
        return appCode;
    }

    /**
     * Sets the value of the appCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppCode(String value) {
        this.appCode = value;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the localText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the localText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocalText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocaleText }
     * 
     * 
     */
    public List<LocaleText> getLocalText() {
        if (localText == null) {
            localText = new ArrayList<LocaleText>();
        }
        return this.localText;
    }

    /**
     * Gets the value of the tags property.
     * 
     * @return
     *     possible object is
     *     {@link LabelTagsList }
     *     
     */
    public LabelTagsList getTags() {
        return tags;
    }

    /**
     * Sets the value of the tags property.
     * 
     * @param value
     *     allowed object is
     *     {@link LabelTagsList }
     *     
     */
    public void setTags(LabelTagsList value) {
        this.tags = value;
    }

}
