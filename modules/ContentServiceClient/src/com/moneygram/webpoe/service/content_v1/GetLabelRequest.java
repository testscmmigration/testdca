
package com.moneygram.webpoe.service.content_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.moneygram.common_v1.BaseServiceRequestMessage;


/**
 * <p>Java class for GetLabelRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetLabelRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceRequestMessage">
 *       &lt;sequence>
 *         &lt;element name="bsnsCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="languageShort" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="applicationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="countryAbbrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="attributesMap" type="{http://moneygram.com/webpoe/service/content_v1}AttributesMap" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetLabelRequest", propOrder = {
    "bsnsCode",
    "languageShort",
    "applicationCode",
    "countryAbbrCode",
    "attributesMap"
})
public class GetLabelRequest
    extends BaseServiceRequestMessage
{

    @XmlElement(required = true)
    protected String bsnsCode;
    @XmlElement(required = true)
    protected String languageShort;
    @XmlElement(required = true)
    protected String applicationCode;
    @XmlElement(required = true)
    protected String countryAbbrCode;
    protected AttributesMap attributesMap;

    /**
     * Gets the value of the bsnsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBsnsCode() {
        return bsnsCode;
    }

    /**
     * Sets the value of the bsnsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBsnsCode(String value) {
        this.bsnsCode = value;
    }

    /**
     * Gets the value of the languageShort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguageShort() {
        return languageShort;
    }

    /**
     * Sets the value of the languageShort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguageShort(String value) {
        this.languageShort = value;
    }

    /**
     * Gets the value of the applicationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationCode() {
        return applicationCode;
    }

    /**
     * Sets the value of the applicationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationCode(String value) {
        this.applicationCode = value;
    }

    /**
     * Gets the value of the countryAbbrCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryAbbrCode() {
        return countryAbbrCode;
    }

    /**
     * Sets the value of the countryAbbrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryAbbrCode(String value) {
        this.countryAbbrCode = value;
    }

    /**
     * Gets the value of the attributesMap property.
     * 
     * @return
     *     possible object is
     *     {@link AttributesMap }
     *     
     */
    public AttributesMap getAttributesMap() {
        return attributesMap;
    }

    /**
     * Sets the value of the attributesMap property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributesMap }
     *     
     */
    public void setAttributesMap(AttributesMap value) {
        this.attributesMap = value;
    }

}
