
package com.moneygram.webpoe.service.content_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.moneygram.common_v1.BaseServiceRequestMessage;


/**
 * <p>Java class for GetContentKeyRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetContentKeyRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceRequestMessage">
 *       &lt;sequence>
 *         &lt;element name="contentItemName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="attributesMap" type="{http://moneygram.com/webpoe/service/content_v1}AttributesMap" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetContentKeyRequest", propOrder = {
    "contentItemName",
    "attributesMap"
})
public class GetContentKeyRequest
    extends BaseServiceRequestMessage
{

    @XmlElement(required = true)
    protected String contentItemName;
    protected AttributesMap attributesMap;

    /**
     * Gets the value of the contentItemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentItemName() {
        return contentItemName;
    }

    /**
     * Sets the value of the contentItemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentItemName(String value) {
        this.contentItemName = value;
    }

    /**
     * Gets the value of the attributesMap property.
     * 
     * @return
     *     possible object is
     *     {@link AttributesMap }
     *     
     */
    public AttributesMap getAttributesMap() {
        return attributesMap;
    }

    /**
     * Sets the value of the attributesMap property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributesMap }
     *     
     */
    public void setAttributesMap(AttributesMap value) {
        this.attributesMap = value;
    }

}
