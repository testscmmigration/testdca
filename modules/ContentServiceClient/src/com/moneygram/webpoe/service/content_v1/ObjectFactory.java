
package com.moneygram.webpoe.service.content_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.moneygram.webpoe.service.content_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetSupportedLanguagesResponse_QNAME = new QName("http://moneygram.com/webpoe/service/content_v1", "getSupportedLanguagesResponse");
    private final static QName _GetApplicationsRequest_QNAME = new QName("http://moneygram.com/webpoe/service/content_v1", "getApplicationsRequest");
    private final static QName _GetLabelResponse_QNAME = new QName("http://moneygram.com/webpoe/service/content_v1", "getLabelResponse");
    private final static QName _GetLabelRequest_QNAME = new QName("http://moneygram.com/webpoe/service/content_v1", "getLabelRequest");
    private final static QName _GetApplicationsResponse_QNAME = new QName("http://moneygram.com/webpoe/service/content_v1", "getApplicationsResponse");
    private final static QName _GetContentKeyResponse_QNAME = new QName("http://moneygram.com/webpoe/service/content_v1", "getContentKeyResponse");
    private final static QName _GetContentKeyRequest_QNAME = new QName("http://moneygram.com/webpoe/service/content_v1", "getContentKeyRequest");
    private final static QName _GetSupportedLanguagesRequest_QNAME = new QName("http://moneygram.com/webpoe/service/content_v1", "getSupportedLanguagesRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.moneygram.webpoe.service.content_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Application }
     * 
     */
    public Application createApplication() {
        return new Application();
    }

    /**
     * Create an instance of {@link GetApplicationsRequest }
     * 
     */
    public GetApplicationsRequest createGetApplicationsRequest() {
        return new GetApplicationsRequest();
    }

    /**
     * Create an instance of {@link GetContentKeyRequest }
     * 
     */
    public GetContentKeyRequest createGetContentKeyRequest() {
        return new GetContentKeyRequest();
    }

    /**
     * Create an instance of {@link LanguageList }
     * 
     */
    public LanguageList createLanguageList() {
        return new LanguageList();
    }

    /**
     * Create an instance of {@link LabelTagsList }
     * 
     */
    public LabelTagsList createLabelTagsList() {
        return new LabelTagsList();
    }

    /**
     * Create an instance of {@link GetApplicationsResponse }
     * 
     */
    public GetApplicationsResponse createGetApplicationsResponse() {
        return new GetApplicationsResponse();
    }

    /**
     * Create an instance of {@link Language }
     * 
     */
    public Language createLanguage() {
        return new Language();
    }

    /**
     * Create an instance of {@link GetContentKeyResponse }
     * 
     */
    public GetContentKeyResponse createGetContentKeyResponse() {
        return new GetContentKeyResponse();
    }

    /**
     * Create an instance of {@link GetLabelResponse }
     * 
     */
    public GetLabelResponse createGetLabelResponse() {
        return new GetLabelResponse();
    }

    /**
     * Create an instance of {@link LabelList }
     * 
     */
    public LabelList createLabelList() {
        return new LabelList();
    }

    /**
     * Create an instance of {@link LocaleText }
     * 
     */
    public LocaleText createLocaleText() {
        return new LocaleText();
    }

    /**
     * Create an instance of {@link Label }
     * 
     */
    public Label createLabel() {
        return new Label();
    }

    /**
     * Create an instance of {@link Attributes }
     * 
     */
    public Attributes createAttributes() {
        return new Attributes();
    }

    /**
     * Create an instance of {@link GetSupportedLanguagesResponse }
     * 
     */
    public GetSupportedLanguagesResponse createGetSupportedLanguagesResponse() {
        return new GetSupportedLanguagesResponse();
    }

    /**
     * Create an instance of {@link AttributesMap }
     * 
     */
    public AttributesMap createAttributesMap() {
        return new AttributesMap();
    }

    /**
     * Create an instance of {@link GetSupportedLanguagesRequest }
     * 
     */
    public GetSupportedLanguagesRequest createGetSupportedLanguagesRequest() {
        return new GetSupportedLanguagesRequest();
    }

    /**
     * Create an instance of {@link GetLabelRequest }
     * 
     */
    public GetLabelRequest createGetLabelRequest() {
        return new GetLabelRequest();
    }

    /**
     * Create an instance of {@link ApplicationList }
     * 
     */
    public ApplicationList createApplicationList() {
        return new ApplicationList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSupportedLanguagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://moneygram.com/webpoe/service/content_v1", name = "getSupportedLanguagesResponse")
    public JAXBElement<GetSupportedLanguagesResponse> createGetSupportedLanguagesResponse(GetSupportedLanguagesResponse value) {
        return new JAXBElement<GetSupportedLanguagesResponse>(_GetSupportedLanguagesResponse_QNAME, GetSupportedLanguagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetApplicationsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://moneygram.com/webpoe/service/content_v1", name = "getApplicationsRequest")
    public JAXBElement<GetApplicationsRequest> createGetApplicationsRequest(GetApplicationsRequest value) {
        return new JAXBElement<GetApplicationsRequest>(_GetApplicationsRequest_QNAME, GetApplicationsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLabelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://moneygram.com/webpoe/service/content_v1", name = "getLabelResponse")
    public JAXBElement<GetLabelResponse> createGetLabelResponse(GetLabelResponse value) {
        return new JAXBElement<GetLabelResponse>(_GetLabelResponse_QNAME, GetLabelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLabelRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://moneygram.com/webpoe/service/content_v1", name = "getLabelRequest")
    public JAXBElement<GetLabelRequest> createGetLabelRequest(GetLabelRequest value) {
        return new JAXBElement<GetLabelRequest>(_GetLabelRequest_QNAME, GetLabelRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetApplicationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://moneygram.com/webpoe/service/content_v1", name = "getApplicationsResponse")
    public JAXBElement<GetApplicationsResponse> createGetApplicationsResponse(GetApplicationsResponse value) {
        return new JAXBElement<GetApplicationsResponse>(_GetApplicationsResponse_QNAME, GetApplicationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetContentKeyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://moneygram.com/webpoe/service/content_v1", name = "getContentKeyResponse")
    public JAXBElement<GetContentKeyResponse> createGetContentKeyResponse(GetContentKeyResponse value) {
        return new JAXBElement<GetContentKeyResponse>(_GetContentKeyResponse_QNAME, GetContentKeyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetContentKeyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://moneygram.com/webpoe/service/content_v1", name = "getContentKeyRequest")
    public JAXBElement<GetContentKeyRequest> createGetContentKeyRequest(GetContentKeyRequest value) {
        return new JAXBElement<GetContentKeyRequest>(_GetContentKeyRequest_QNAME, GetContentKeyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSupportedLanguagesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://moneygram.com/webpoe/service/content_v1", name = "getSupportedLanguagesRequest")
    public JAXBElement<GetSupportedLanguagesRequest> createGetSupportedLanguagesRequest(GetSupportedLanguagesRequest value) {
        return new JAXBElement<GetSupportedLanguagesRequest>(_GetSupportedLanguagesRequest_QNAME, GetSupportedLanguagesRequest.class, null, value);
    }

}
