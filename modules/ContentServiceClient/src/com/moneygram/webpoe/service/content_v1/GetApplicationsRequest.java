
package com.moneygram.webpoe.service.content_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.moneygram.common_v1.BaseServiceRequestMessage;


/**
 * <p>Java class for GetApplicationsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetApplicationsRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceRequestMessage">
 *       &lt;sequence>
 *         &lt;element name="bsnsCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetApplicationsRequest", propOrder = {
    "bsnsCode"
})
public class GetApplicationsRequest
    extends BaseServiceRequestMessage
{

    @XmlElement(required = true)
    protected String bsnsCode;

    /**
     * Gets the value of the bsnsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBsnsCode() {
        return bsnsCode;
    }

    /**
     * Sets the value of the bsnsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBsnsCode(String value) {
        this.bsnsCode = value;
    }

}
